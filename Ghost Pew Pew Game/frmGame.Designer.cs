﻿namespace Ghost_Pew_Pew_Game
{
    partial class frmGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGame));
            this.tmrMovement = new System.Windows.Forms.Timer(this.components);
            this.tmrP1Parry = new System.Windows.Forms.Timer(this.components);
            this.tmrP2Parry = new System.Windows.Forms.Timer(this.components);
            this.tmrP1StaminaCooldown = new System.Windows.Forms.Timer(this.components);
            this.tmrP2StaminaCooldown = new System.Windows.Forms.Timer(this.components);
            this.pgbP1Health = new System.Windows.Forms.ProgressBar();
            this.pgbP2Stamina = new System.Windows.Forms.ProgressBar();
            this.pgbP2Health = new System.Windows.Forms.ProgressBar();
            this.pgbP1Stamina = new System.Windows.Forms.ProgressBar();
            this.tmrStamina = new System.Windows.Forms.Timer(this.components);
            this.tmrAnimation = new System.Windows.Forms.Timer(this.components);
            this.lblGameTime = new System.Windows.Forms.Label();
            this.tmrGameTime = new System.Windows.Forms.Timer(this.components);
            this.tmrIntro = new System.Windows.Forms.Timer(this.components);
            this.lblCountdown = new System.Windows.Forms.Label();
            this.pbxP2 = new System.Windows.Forms.PictureBox();
            this.pbxP1 = new System.Windows.Forms.PictureBox();
            this.pbxBarHolder = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxBarHolder)).BeginInit();
            this.SuspendLayout();
            // 
            // tmrMovement
            // 
            this.tmrMovement.Interval = 1;
            this.tmrMovement.Tick += new System.EventHandler(this.tmrMovement_Tick);
            // 
            // tmrP1Parry
            // 
            this.tmrP1Parry.Tick += new System.EventHandler(this.tmrP1Parry_Tick);
            // 
            // tmrP2Parry
            // 
            this.tmrP2Parry.Tick += new System.EventHandler(this.tmrP2Parry_Tick);
            // 
            // tmrP1StaminaCooldown
            // 
            this.tmrP1StaminaCooldown.Interval = 3500;
            this.tmrP1StaminaCooldown.Tick += new System.EventHandler(this.tmrP1StaminaCooldown_Tick);
            // 
            // tmrP2StaminaCooldown
            // 
            this.tmrP2StaminaCooldown.Interval = 3500;
            this.tmrP2StaminaCooldown.Tick += new System.EventHandler(this.tmrP2StaminaCooldown_Tick);
            // 
            // pgbP1Health
            // 
            this.pgbP1Health.ForeColor = System.Drawing.Color.Red;
            this.pgbP1Health.Location = new System.Drawing.Point(189, 41);
            this.pgbP1Health.Name = "pgbP1Health";
            this.pgbP1Health.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.pgbP1Health.RightToLeftLayout = true;
            this.pgbP1Health.Size = new System.Drawing.Size(360, 30);
            this.pgbP1Health.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pgbP1Health.TabIndex = 2;
            // 
            // pgbP2Stamina
            // 
            this.pgbP2Stamina.ForeColor = System.Drawing.Color.Lime;
            this.pgbP2Stamina.Location = new System.Drawing.Point(716, 83);
            this.pgbP2Stamina.Name = "pgbP2Stamina";
            this.pgbP2Stamina.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.pgbP2Stamina.Size = new System.Drawing.Size(330, 20);
            this.pgbP2Stamina.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pgbP2Stamina.TabIndex = 3;
            // 
            // pgbP2Health
            // 
            this.pgbP2Health.ForeColor = System.Drawing.Color.Red;
            this.pgbP2Health.Location = new System.Drawing.Point(716, 41);
            this.pgbP2Health.Name = "pgbP2Health";
            this.pgbP2Health.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.pgbP2Health.Size = new System.Drawing.Size(360, 30);
            this.pgbP2Health.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pgbP2Health.TabIndex = 4;
            // 
            // pgbP1Stamina
            // 
            this.pgbP1Stamina.ForeColor = System.Drawing.Color.Lime;
            this.pgbP1Stamina.Location = new System.Drawing.Point(219, 83);
            this.pgbP1Stamina.Name = "pgbP1Stamina";
            this.pgbP1Stamina.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.pgbP1Stamina.RightToLeftLayout = true;
            this.pgbP1Stamina.Size = new System.Drawing.Size(330, 20);
            this.pgbP1Stamina.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pgbP1Stamina.TabIndex = 5;
            // 
            // tmrStamina
            // 
            this.tmrStamina.Tick += new System.EventHandler(this.tmrStamina_Tick);
            // 
            // tmrAnimation
            // 
            this.tmrAnimation.Interval = 200;
            this.tmrAnimation.Tick += new System.EventHandler(this.tmrAnimation_Tick);
            // 
            // lblGameTime
            // 
            this.lblGameTime.AutoSize = true;
            this.lblGameTime.BackColor = System.Drawing.Color.Transparent;
            this.lblGameTime.Font = new System.Drawing.Font("Harrington", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGameTime.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lblGameTime.Location = new System.Drawing.Point(496, 41);
            this.lblGameTime.Name = "lblGameTime";
            this.lblGameTime.Size = new System.Drawing.Size(101, 76);
            this.lblGameTime.TabIndex = 7;
            this.lblGameTime.Text = "99";
            // 
            // tmrGameTime
            // 
            this.tmrGameTime.Interval = 1000;
            this.tmrGameTime.Tick += new System.EventHandler(this.tmrGameTime_Tick);
            // 
            // tmrIntro
            // 
            this.tmrIntro.Interval = 1000;
            this.tmrIntro.Tick += new System.EventHandler(this.tmrIntro_Tick);
            // 
            // lblCountdown
            // 
            this.lblCountdown.AutoSize = true;
            this.lblCountdown.BackColor = System.Drawing.Color.Transparent;
            this.lblCountdown.Font = new System.Drawing.Font("Harrington", 300F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCountdown.ForeColor = System.Drawing.Color.White;
            this.lblCountdown.Location = new System.Drawing.Point(430, 150);
            this.lblCountdown.Name = "lblCountdown";
            this.lblCountdown.Size = new System.Drawing.Size(0, 470);
            this.lblCountdown.TabIndex = 8;
            this.lblCountdown.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pbxP2
            // 
            this.pbxP2.BackColor = System.Drawing.Color.Transparent;
            this.pbxP2.Location = new System.Drawing.Point(932, 199);
            this.pbxP2.Name = "pbxP2";
            this.pbxP2.Size = new System.Drawing.Size(114, 143);
            this.pbxP2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxP2.TabIndex = 1;
            this.pbxP2.TabStop = false;
            // 
            // pbxP1
            // 
            this.pbxP1.BackColor = System.Drawing.Color.Transparent;
            this.pbxP1.Location = new System.Drawing.Point(219, 199);
            this.pbxP1.Name = "pbxP1";
            this.pbxP1.Size = new System.Drawing.Size(114, 143);
            this.pbxP1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxP1.TabIndex = 0;
            this.pbxP1.TabStop = false;
            // 
            // pbxBarHolder
            // 
            this.pbxBarHolder.BackColor = System.Drawing.Color.Transparent;
            this.pbxBarHolder.Image = ((System.Drawing.Image)(resources.GetObject("pbxBarHolder.Image")));
            this.pbxBarHolder.Location = new System.Drawing.Point(91, 1);
            this.pbxBarHolder.Name = "pbxBarHolder";
            this.pbxBarHolder.Size = new System.Drawing.Size(1083, 146);
            this.pbxBarHolder.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pbxBarHolder.TabIndex = 6;
            this.pbxBarHolder.TabStop = false;
            // 
            // frmGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(1264, 682);
            this.Controls.Add(this.lblCountdown);
            this.Controls.Add(this.lblGameTime);
            this.Controls.Add(this.pgbP1Stamina);
            this.Controls.Add(this.pgbP2Health);
            this.Controls.Add(this.pgbP2Stamina);
            this.Controls.Add(this.pgbP1Health);
            this.Controls.Add(this.pbxP2);
            this.Controls.Add(this.pbxP1);
            this.Controls.Add(this.pbxBarHolder);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmGame";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ghost Pew Pew - Game";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmGame_FormClosing);
            this.Load += new System.EventHandler(this.frmGame_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.frmGame_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmGame_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmGame_KeyUp);
            ((System.ComponentModel.ISupportInitialize)(this.pbxP2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxBarHolder)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbxP1;
        private System.Windows.Forms.Timer tmrMovement;
        private System.Windows.Forms.Timer tmrP1Parry;
        private System.Windows.Forms.Timer tmrP2Parry;
        private System.Windows.Forms.Timer tmrP1StaminaCooldown;
        private System.Windows.Forms.Timer tmrP2StaminaCooldown;
        private System.Windows.Forms.PictureBox pbxP2;
        private System.Windows.Forms.ProgressBar pgbP1Health;
        private System.Windows.Forms.ProgressBar pgbP2Stamina;
        private System.Windows.Forms.ProgressBar pgbP2Health;
        private System.Windows.Forms.ProgressBar pgbP1Stamina;
        private System.Windows.Forms.Timer tmrStamina;
        private System.Windows.Forms.Timer tmrAnimation;
        private System.Windows.Forms.PictureBox pbxBarHolder;
        private System.Windows.Forms.Label lblGameTime;
        private System.Windows.Forms.Timer tmrGameTime;
        private System.Windows.Forms.Timer tmrIntro;
        private System.Windows.Forms.Label lblCountdown;
    }
}

