﻿namespace Ghost_Pew_Pew_Game
{
    partial class frmHelp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmHelp));
            this.tpHelp = new System.Windows.Forms.TabControl();
            this.tpControls = new System.Windows.Forms.TabPage();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.pictureBox35 = new System.Windows.Forms.PictureBox();
            this.pictureBox34 = new System.Windows.Forms.PictureBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.pictureBox32 = new System.Windows.Forms.PictureBox();
            this.pictureBox33 = new System.Windows.Forms.PictureBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.pictureBox28 = new System.Windows.Forms.PictureBox();
            this.pictureBox29 = new System.Windows.Forms.PictureBox();
            this.pictureBox30 = new System.Windows.Forms.PictureBox();
            this.pictureBox31 = new System.Windows.Forms.PictureBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.label28 = new System.Windows.Forms.Label();
            this.tpMovement = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tpFireballs = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.tpBlock = new System.Windows.Forms.TabPage();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.tpParry = new System.Windows.Forms.TabPage();
            this.label32 = new System.Windows.Forms.Label();
            this.pictureBox39 = new System.Windows.Forms.PictureBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.pictureBox37 = new System.Windows.Forms.PictureBox();
            this.pictureBox38 = new System.Windows.Forms.PictureBox();
            this.tpInterface = new System.Windows.Forms.TabPage();
            this.pictureBox36 = new System.Windows.Forms.PictureBox();
            this.tpAttributes = new System.Windows.Forms.TabPage();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tpMechanics = new System.Windows.Forms.TabPage();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.pbxBack = new System.Windows.Forms.PictureBox();
            this.tpHelp.SuspendLayout();
            this.tpControls.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            this.tpMovement.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tpFireballs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            this.tpBlock.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            this.tpParry.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox38)).BeginInit();
            this.tpInterface.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).BeginInit();
            this.tpAttributes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.tpMechanics.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxBack)).BeginInit();
            this.SuspendLayout();
            // 
            // tpHelp
            // 
            this.tpHelp.Controls.Add(this.tpControls);
            this.tpHelp.Controls.Add(this.tpMovement);
            this.tpHelp.Controls.Add(this.tpFireballs);
            this.tpHelp.Controls.Add(this.tpBlock);
            this.tpHelp.Controls.Add(this.tpParry);
            this.tpHelp.Controls.Add(this.tpInterface);
            this.tpHelp.Controls.Add(this.tpAttributes);
            this.tpHelp.Controls.Add(this.tpMechanics);
            this.tpHelp.Font = new System.Drawing.Font("Harrington", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpHelp.Location = new System.Drawing.Point(208, 12);
            this.tpHelp.Name = "tpHelp";
            this.tpHelp.SelectedIndex = 0;
            this.tpHelp.Size = new System.Drawing.Size(1044, 658);
            this.tpHelp.TabIndex = 1;
            // 
            // tpControls
            // 
            this.tpControls.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.tpControls.Controls.Add(this.label21);
            this.tpControls.Controls.Add(this.label22);
            this.tpControls.Controls.Add(this.label26);
            this.tpControls.Controls.Add(this.label29);
            this.tpControls.Controls.Add(this.pictureBox35);
            this.tpControls.Controls.Add(this.pictureBox34);
            this.tpControls.Controls.Add(this.label24);
            this.tpControls.Controls.Add(this.label25);
            this.tpControls.Controls.Add(this.label27);
            this.tpControls.Controls.Add(this.pictureBox32);
            this.tpControls.Controls.Add(this.pictureBox33);
            this.tpControls.Controls.Add(this.label20);
            this.tpControls.Controls.Add(this.label23);
            this.tpControls.Controls.Add(this.pictureBox28);
            this.tpControls.Controls.Add(this.pictureBox29);
            this.tpControls.Controls.Add(this.pictureBox30);
            this.tpControls.Controls.Add(this.pictureBox31);
            this.tpControls.Controls.Add(this.label18);
            this.tpControls.Controls.Add(this.label19);
            this.tpControls.Controls.Add(this.pictureBox16);
            this.tpControls.Controls.Add(this.pictureBox20);
            this.tpControls.Controls.Add(this.pictureBox22);
            this.tpControls.Controls.Add(this.pictureBox23);
            this.tpControls.Controls.Add(this.pictureBox24);
            this.tpControls.Controls.Add(this.pictureBox25);
            this.tpControls.Controls.Add(this.pictureBox26);
            this.tpControls.Controls.Add(this.pictureBox27);
            this.tpControls.Controls.Add(this.label28);
            this.tpControls.Location = new System.Drawing.Point(4, 28);
            this.tpControls.Name = "tpControls";
            this.tpControls.Size = new System.Drawing.Size(1036, 626);
            this.tpControls.TabIndex = 5;
            this.tpControls.Text = "Control Summary";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(544, 354);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(106, 41);
            this.label21.TabIndex = 49;
            this.label21.Text = "Block";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(384, 354);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(107, 41);
            this.label22.TabIndex = 48;
            this.label22.Text = "Parry";
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(876, 354);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(140, 86);
            this.label26.TabIndex = 47;
            this.label26.Text = "Light Fireball";
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(710, 354);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(140, 86);
            this.label29.TabIndex = 50;
            this.label29.Text = "Heavy Fireball";
            // 
            // pictureBox35
            // 
            this.pictureBox35.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox35.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.L;
            this.pictureBox35.Location = new System.Drawing.Point(883, 185);
            this.pictureBox35.Name = "pictureBox35";
            this.pictureBox35.Size = new System.Drawing.Size(89, 74);
            this.pictureBox35.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox35.TabIndex = 45;
            this.pictureBox35.TabStop = false;
            // 
            // pictureBox34
            // 
            this.pictureBox34.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox34.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.C;
            this.pictureBox34.Location = new System.Drawing.Point(391, 473);
            this.pictureBox34.Name = "pictureBox34";
            this.pictureBox34.Size = new System.Drawing.Size(89, 74);
            this.pictureBox34.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox34.TabIndex = 44;
            this.pictureBox34.TabStop = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(549, 550);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(112, 41);
            this.label24.TabIndex = 43;
            this.label24.Text = "(Hold)";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(715, 262);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(112, 41);
            this.label25.TabIndex = 42;
            this.label25.Text = "(Hold)";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(710, 86);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(106, 41);
            this.label27.TabIndex = 40;
            this.label27.Text = "Block";
            // 
            // pictureBox32
            // 
            this.pictureBox32.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox32.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.V;
            this.pictureBox32.Location = new System.Drawing.Point(551, 473);
            this.pictureBox32.Name = "pictureBox32";
            this.pictureBox32.Size = new System.Drawing.Size(89, 74);
            this.pictureBox32.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox32.TabIndex = 39;
            this.pictureBox32.TabStop = false;
            // 
            // pictureBox33
            // 
            this.pictureBox33.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox33.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.K;
            this.pictureBox33.Location = new System.Drawing.Point(717, 185);
            this.pictureBox33.Name = "pictureBox33";
            this.pictureBox33.Size = new System.Drawing.Size(89, 74);
            this.pictureBox33.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox33.TabIndex = 38;
            this.pictureBox33.TabStop = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(876, 86);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(107, 41);
            this.label20.TabIndex = 34;
            this.label20.Text = "Parry";
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(378, 86);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(140, 86);
            this.label23.TabIndex = 31;
            this.label23.Text = "Light Fireball";
            // 
            // pictureBox28
            // 
            this.pictureBox28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox28.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.N;
            this.pictureBox28.Location = new System.Drawing.Point(883, 473);
            this.pictureBox28.Name = "pictureBox28";
            this.pictureBox28.Size = new System.Drawing.Size(89, 74);
            this.pictureBox28.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox28.TabIndex = 30;
            this.pictureBox28.TabStop = false;
            // 
            // pictureBox29
            // 
            this.pictureBox29.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox29.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.J;
            this.pictureBox29.Location = new System.Drawing.Point(557, 188);
            this.pictureBox29.Name = "pictureBox29";
            this.pictureBox29.Size = new System.Drawing.Size(89, 74);
            this.pictureBox29.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox29.TabIndex = 29;
            this.pictureBox29.TabStop = false;
            // 
            // pictureBox30
            // 
            this.pictureBox30.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox30.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.B;
            this.pictureBox30.Location = new System.Drawing.Point(717, 473);
            this.pictureBox30.Name = "pictureBox30";
            this.pictureBox30.Size = new System.Drawing.Size(89, 74);
            this.pictureBox30.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox30.TabIndex = 28;
            this.pictureBox30.TabStop = false;
            // 
            // pictureBox31
            // 
            this.pictureBox31.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox31.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.H;
            this.pictureBox31.Location = new System.Drawing.Point(385, 188);
            this.pictureBox31.Name = "pictureBox31";
            this.pictureBox31.Size = new System.Drawing.Size(89, 74);
            this.pictureBox31.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox31.TabIndex = 27;
            this.pictureBox31.TabStop = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(35, 349);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(146, 41);
            this.label18.TabIndex = 20;
            this.label18.Text = "Player 2";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(18, 64);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(140, 41);
            this.label19.TabIndex = 19;
            this.label19.Text = "Player 1";
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox16.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Left;
            this.pictureBox16.Location = new System.Drawing.Point(42, 473);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(89, 74);
            this.pictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox16.TabIndex = 18;
            this.pictureBox16.TabStop = false;
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox20.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Down;
            this.pictureBox20.Location = new System.Drawing.Point(137, 473);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(89, 74);
            this.pictureBox20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox20.TabIndex = 17;
            this.pictureBox20.TabStop = false;
            // 
            // pictureBox22
            // 
            this.pictureBox22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox22.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Right;
            this.pictureBox22.Location = new System.Drawing.Point(232, 473);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(89, 74);
            this.pictureBox22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox22.TabIndex = 16;
            this.pictureBox22.TabStop = false;
            // 
            // pictureBox23
            // 
            this.pictureBox23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox23.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Up;
            this.pictureBox23.Location = new System.Drawing.Point(137, 393);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(89, 74);
            this.pictureBox23.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox23.TabIndex = 15;
            this.pictureBox23.TabStop = false;
            // 
            // pictureBox24
            // 
            this.pictureBox24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox24.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.A;
            this.pictureBox24.Location = new System.Drawing.Point(41, 188);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(89, 74);
            this.pictureBox24.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox24.TabIndex = 14;
            this.pictureBox24.TabStop = false;
            // 
            // pictureBox25
            // 
            this.pictureBox25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox25.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.S;
            this.pictureBox25.Location = new System.Drawing.Point(136, 188);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(89, 74);
            this.pictureBox25.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox25.TabIndex = 13;
            this.pictureBox25.TabStop = false;
            // 
            // pictureBox26
            // 
            this.pictureBox26.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox26.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.D;
            this.pictureBox26.Location = new System.Drawing.Point(231, 188);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(89, 74);
            this.pictureBox26.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox26.TabIndex = 12;
            this.pictureBox26.TabStop = false;
            // 
            // pictureBox27
            // 
            this.pictureBox27.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox27.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.W;
            this.pictureBox27.Location = new System.Drawing.Point(136, 108);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(89, 74);
            this.pictureBox27.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox27.TabIndex = 11;
            this.pictureBox27.TabStop = false;
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(548, 86);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(140, 86);
            this.label28.TabIndex = 46;
            this.label28.Text = "Heavy Fireball";
            // 
            // tpMovement
            // 
            this.tpMovement.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.tpMovement.Controls.Add(this.label3);
            this.tpMovement.Controls.Add(this.label2);
            this.tpMovement.Controls.Add(this.label1);
            this.tpMovement.Controls.Add(this.pictureBox6);
            this.tpMovement.Controls.Add(this.pictureBox7);
            this.tpMovement.Controls.Add(this.pictureBox8);
            this.tpMovement.Controls.Add(this.pictureBox9);
            this.tpMovement.Controls.Add(this.pictureBox5);
            this.tpMovement.Controls.Add(this.pictureBox4);
            this.tpMovement.Controls.Add(this.pictureBox3);
            this.tpMovement.Controls.Add(this.pictureBox2);
            this.tpMovement.Controls.Add(this.pictureBox1);
            this.tpMovement.ForeColor = System.Drawing.Color.Black;
            this.tpMovement.Location = new System.Drawing.Point(4, 28);
            this.tpMovement.Name = "tpMovement";
            this.tpMovement.Padding = new System.Windows.Forms.Padding(3);
            this.tpMovement.Size = new System.Drawing.Size(1036, 626);
            this.tpMovement.TabIndex = 0;
            this.tpMovement.Text = "Movement";
            this.tpMovement.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(420, 384);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(610, 167);
            this.label3.TabIndex = 11;
            this.label3.Text = "Players will use their respective keys to move their ghost";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(57, 327);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(146, 41);
            this.label2.TabIndex = 10;
            this.label2.Text = "Player 2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(41, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 41);
            this.label1.TabIndex = 9;
            this.label1.Text = "Player 1";
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox6.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Left;
            this.pictureBox6.Location = new System.Drawing.Point(64, 451);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(89, 74);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 8;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox7.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Down;
            this.pictureBox7.Location = new System.Drawing.Point(159, 451);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(89, 74);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox7.TabIndex = 7;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox8.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Right;
            this.pictureBox8.Location = new System.Drawing.Point(254, 451);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(89, 74);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox8.TabIndex = 6;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox9.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Up;
            this.pictureBox9.Location = new System.Drawing.Point(159, 371);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(89, 74);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox9.TabIndex = 5;
            this.pictureBox9.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox5.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.A;
            this.pictureBox5.Location = new System.Drawing.Point(64, 149);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(89, 74);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 4;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox4.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.S;
            this.pictureBox4.Location = new System.Drawing.Point(159, 149);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(89, 74);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox3.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.D;
            this.pictureBox3.Location = new System.Drawing.Point(254, 149);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(89, 74);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Help1;
            this.pictureBox2.Location = new System.Drawing.Point(420, 6);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(610, 375);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.W;
            this.pictureBox1.Location = new System.Drawing.Point(159, 69);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(89, 74);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // tpFireballs
            // 
            this.tpFireballs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.tpFireballs.Controls.Add(this.label7);
            this.tpFireballs.Controls.Add(this.label8);
            this.tpFireballs.Controls.Add(this.pictureBox19);
            this.tpFireballs.Controls.Add(this.label4);
            this.tpFireballs.Controls.Add(this.label5);
            this.tpFireballs.Controls.Add(this.label6);
            this.tpFireballs.Controls.Add(this.pictureBox13);
            this.tpFireballs.Controls.Add(this.pictureBox14);
            this.tpFireballs.Controls.Add(this.pictureBox15);
            this.tpFireballs.Controls.Add(this.pictureBox17);
            this.tpFireballs.Controls.Add(this.pictureBox18);
            this.tpFireballs.Location = new System.Drawing.Point(4, 28);
            this.tpFireballs.Name = "tpFireballs";
            this.tpFireballs.Padding = new System.Windows.Forms.Padding(3);
            this.tpFireballs.Size = new System.Drawing.Size(1036, 626);
            this.tpFireballs.TabIndex = 1;
            this.tpFireballs.Text = "Fireballs";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(235, 232);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(146, 41);
            this.label7.TabIndex = 26;
            this.label7.Text = "Player 2";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(41, 232);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(140, 41);
            this.label8.TabIndex = 25;
            this.label8.Text = "Player 1";
            // 
            // pictureBox19
            // 
            this.pictureBox19.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Help3;
            this.pictureBox19.Location = new System.Drawing.Point(420, 250);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(589, 238);
            this.pictureBox19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox19.TabIndex = 24;
            this.pictureBox19.TabStop = false;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(48, 491);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(961, 127);
            this.label4.TabIndex = 23;
            this.label4.Text = "The large fireballs do more damage but cost more stamina to shoot";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(235, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(146, 41);
            this.label5.TabIndex = 22;
            this.label5.Text = "Player 2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(41, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(140, 41);
            this.label6.TabIndex = 21;
            this.label6.Text = "Player 1";
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox13.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.N;
            this.pictureBox13.Location = new System.Drawing.Point(232, 69);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(89, 74);
            this.pictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox13.TabIndex = 17;
            this.pictureBox13.TabStop = false;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox14.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.J;
            this.pictureBox14.Location = new System.Drawing.Point(55, 276);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(89, 74);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox14.TabIndex = 16;
            this.pictureBox14.TabStop = false;
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox15.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.B;
            this.pictureBox15.Location = new System.Drawing.Point(232, 276);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(89, 74);
            this.pictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox15.TabIndex = 15;
            this.pictureBox15.TabStop = false;
            // 
            // pictureBox17
            // 
            this.pictureBox17.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Help2;
            this.pictureBox17.Location = new System.Drawing.Point(420, 6);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(589, 238);
            this.pictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox17.TabIndex = 13;
            this.pictureBox17.TabStop = false;
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox18.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.H;
            this.pictureBox18.Location = new System.Drawing.Point(48, 69);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(89, 74);
            this.pictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox18.TabIndex = 12;
            this.pictureBox18.TabStop = false;
            // 
            // tpBlock
            // 
            this.tpBlock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.tpBlock.Controls.Add(this.label10);
            this.tpBlock.Controls.Add(this.label9);
            this.tpBlock.Controls.Add(this.pictureBox10);
            this.tpBlock.Controls.Add(this.label11);
            this.tpBlock.Controls.Add(this.label12);
            this.tpBlock.Controls.Add(this.label13);
            this.tpBlock.Controls.Add(this.pictureBox11);
            this.tpBlock.Controls.Add(this.pictureBox21);
            this.tpBlock.Location = new System.Drawing.Point(4, 28);
            this.tpBlock.Name = "tpBlock";
            this.tpBlock.Size = new System.Drawing.Size(1036, 626);
            this.tpBlock.TabIndex = 2;
            this.tpBlock.Text = "Block";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(450, 203);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(112, 41);
            this.label10.TabIndex = 37;
            this.label10.Text = "(Hold)";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(164, 203);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(112, 41);
            this.label9.TabIndex = 36;
            this.label9.Text = "(Hold)";
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Help4;
            this.pictureBox10.Location = new System.Drawing.Point(575, 20);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(448, 311);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox10.TabIndex = 35;
            this.pictureBox10.TabStop = false;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(42, 363);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(961, 127);
            this.label11.TabIndex = 34;
            this.label11.Text = "Hold the respective key to block. Blocking absorbs the attack and reduces stamina" +
    " in place of health. If there isn\'t enough stamina, the excess will be deducted " +
    "from health.";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(358, 126);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(146, 41);
            this.label12.TabIndex = 33;
            this.label12.Text = "Player 2";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(62, 126);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(140, 41);
            this.label13.TabIndex = 32;
            this.label13.Text = "Player 1";
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox11.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.V;
            this.pictureBox11.Location = new System.Drawing.Point(355, 170);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(89, 74);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox11.TabIndex = 31;
            this.pictureBox11.TabStop = false;
            // 
            // pictureBox21
            // 
            this.pictureBox21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox21.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.K;
            this.pictureBox21.Location = new System.Drawing.Point(69, 170);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(89, 74);
            this.pictureBox21.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox21.TabIndex = 27;
            this.pictureBox21.TabStop = false;
            // 
            // tpParry
            // 
            this.tpParry.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.tpParry.Controls.Add(this.label32);
            this.tpParry.Controls.Add(this.pictureBox39);
            this.tpParry.Controls.Add(this.label30);
            this.tpParry.Controls.Add(this.label31);
            this.tpParry.Controls.Add(this.pictureBox37);
            this.tpParry.Controls.Add(this.pictureBox38);
            this.tpParry.Location = new System.Drawing.Point(4, 28);
            this.tpParry.Name = "tpParry";
            this.tpParry.Size = new System.Drawing.Size(1036, 626);
            this.tpParry.TabIndex = 3;
            this.tpParry.Text = "Parry";
            // 
            // label32
            // 
            this.label32.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(42, 429);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(959, 179);
            this.label32.TabIndex = 37;
            this.label32.Text = "By pressing the respective key before being hit by a projectile, it can be parrie" +
    "d. Depending on what projectile it was, different effects may happen. See \"Detai" +
    "led Mechanics\" for more information.";
            // 
            // pictureBox39
            // 
            this.pictureBox39.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Help9;
            this.pictureBox39.Location = new System.Drawing.Point(452, 3);
            this.pictureBox39.Name = "pictureBox39";
            this.pictureBox39.Size = new System.Drawing.Size(569, 395);
            this.pictureBox39.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox39.TabIndex = 36;
            this.pictureBox39.TabStop = false;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(236, 62);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(146, 41);
            this.label30.TabIndex = 26;
            this.label30.Text = "Player 2";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(42, 62);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(140, 41);
            this.label31.TabIndex = 25;
            this.label31.Text = "Player 1";
            // 
            // pictureBox37
            // 
            this.pictureBox37.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox37.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.C;
            this.pictureBox37.Location = new System.Drawing.Point(233, 106);
            this.pictureBox37.Name = "pictureBox37";
            this.pictureBox37.Size = new System.Drawing.Size(89, 74);
            this.pictureBox37.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox37.TabIndex = 24;
            this.pictureBox37.TabStop = false;
            // 
            // pictureBox38
            // 
            this.pictureBox38.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox38.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.L;
            this.pictureBox38.Location = new System.Drawing.Point(49, 106);
            this.pictureBox38.Name = "pictureBox38";
            this.pictureBox38.Size = new System.Drawing.Size(89, 74);
            this.pictureBox38.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox38.TabIndex = 23;
            this.pictureBox38.TabStop = false;
            // 
            // tpInterface
            // 
            this.tpInterface.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.tpInterface.Controls.Add(this.pictureBox36);
            this.tpInterface.Location = new System.Drawing.Point(4, 28);
            this.tpInterface.Name = "tpInterface";
            this.tpInterface.Size = new System.Drawing.Size(1036, 626);
            this.tpInterface.TabIndex = 4;
            this.tpInterface.Text = "Interface";
            // 
            // pictureBox36
            // 
            this.pictureBox36.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Help7;
            this.pictureBox36.Location = new System.Drawing.Point(23, 67);
            this.pictureBox36.Name = "pictureBox36";
            this.pictureBox36.Size = new System.Drawing.Size(991, 529);
            this.pictureBox36.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox36.TabIndex = 0;
            this.pictureBox36.TabStop = false;
            // 
            // tpAttributes
            // 
            this.tpAttributes.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.tpAttributes.Controls.Add(this.pictureBox12);
            this.tpAttributes.Controls.Add(this.label17);
            this.tpAttributes.Controls.Add(this.label16);
            this.tpAttributes.Controls.Add(this.label15);
            this.tpAttributes.Controls.Add(this.label14);
            this.tpAttributes.Location = new System.Drawing.Point(4, 28);
            this.tpAttributes.Name = "tpAttributes";
            this.tpAttributes.Size = new System.Drawing.Size(1036, 626);
            this.tpAttributes.TabIndex = 6;
            this.tpAttributes.Text = "Attributes";
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Help5;
            this.pictureBox12.Location = new System.Drawing.Point(678, 20);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(343, 575);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox12.TabIndex = 36;
            this.pictureBox12.TabStop = false;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(29, 177);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(643, 136);
            this.label17.TabIndex = 13;
            this.label17.Text = "Stamina affects stamina regeneration and reduces cooldown time when stamina is de" +
    "pleted";
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(29, 342);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(547, 58);
            this.label16.TabIndex = 12;
            this.label16.Text = "Parry increases the parry window";
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(29, 443);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(596, 113);
            this.label15.TabIndex = 11;
            this.label15.Text = "Jump affects the amount of jumps that can be chained in the air";
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Harrington", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(29, 53);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(643, 100);
            this.label14.TabIndex = 10;
            this.label14.Text = "Damage affects the damage of fireballs and ice shards";
            // 
            // tpMechanics
            // 
            this.tpMechanics.Controls.Add(this.textBox1);
            this.tpMechanics.Location = new System.Drawing.Point(4, 28);
            this.tpMechanics.Name = "tpMechanics";
            this.tpMechanics.Size = new System.Drawing.Size(1036, 626);
            this.tpMechanics.TabIndex = 7;
            this.tpMechanics.Text = "Detailed Mechanics";
            this.tpMechanics.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Harrington", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(3, 3);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(1019, 561);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = resources.GetString("textBox1.Text");
            // 
            // pbxBack
            // 
            this.pbxBack.BackColor = System.Drawing.Color.Transparent;
            this.pbxBack.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Back;
            this.pbxBack.Location = new System.Drawing.Point(12, 575);
            this.pbxBack.Name = "pbxBack";
            this.pbxBack.Size = new System.Drawing.Size(190, 96);
            this.pbxBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxBack.TabIndex = 4;
            this.pbxBack.TabStop = false;
            this.pbxBack.Click += new System.EventHandler(this.pbxBack_Click);
            // 
            // frmHelp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = global::Ghost_Pew_Pew_Game.Properties.Resources.HauntedHouse;
            this.ClientSize = new System.Drawing.Size(1264, 682);
            this.Controls.Add(this.pbxBack);
            this.Controls.Add(this.tpHelp);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmHelp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ghost Pew Pew - Help";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmHelp_FormClosing);
            this.tpHelp.ResumeLayout(false);
            this.tpControls.ResumeLayout(false);
            this.tpControls.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            this.tpMovement.ResumeLayout(false);
            this.tpMovement.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tpFireballs.ResumeLayout(false);
            this.tpFireballs.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            this.tpBlock.ResumeLayout(false);
            this.tpBlock.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            this.tpParry.ResumeLayout(false);
            this.tpParry.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox38)).EndInit();
            this.tpInterface.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).EndInit();
            this.tpAttributes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.tpMechanics.ResumeLayout(false);
            this.tpMechanics.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxBack)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tpHelp;
        private System.Windows.Forms.TabPage tpMovement;
        private System.Windows.Forms.TabPage tpFireballs;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.TabPage tpControls;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.TabPage tpBlock;
        private System.Windows.Forms.TabPage tpParry;
        private System.Windows.Forms.TabPage tpInterface;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TabPage tpAttributes;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox35;
        private System.Windows.Forms.PictureBox pictureBox34;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.PictureBox pictureBox32;
        private System.Windows.Forms.PictureBox pictureBox33;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.PictureBox pictureBox28;
        private System.Windows.Forms.PictureBox pictureBox29;
        private System.Windows.Forms.PictureBox pictureBox30;
        private System.Windows.Forms.PictureBox pictureBox31;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.PictureBox pictureBox25;
        private System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.PictureBox pictureBox27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.PictureBox pictureBox36;
        private System.Windows.Forms.TabPage tpMechanics;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.PictureBox pictureBox39;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.PictureBox pictureBox37;
        private System.Windows.Forms.PictureBox pictureBox38;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.PictureBox pbxBack;

    }
}