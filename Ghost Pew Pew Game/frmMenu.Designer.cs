﻿namespace Ghost_Pew_Pew_Game
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMenu));
            this.label1 = new System.Windows.Forms.Label();
            this.pbxGame = new System.Windows.Forms.PictureBox();
            this.pbxBuild = new System.Windows.Forms.PictureBox();
            this.pbxHelp = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbxGame)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxBuild)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHelp)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Harrington", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(53, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(691, 113);
            this.label1.TabIndex = 2;
            this.label1.Text = "Ghost Pew Pew";
            // 
            // pbxGame
            // 
            this.pbxGame.BackColor = System.Drawing.Color.Transparent;
            this.pbxGame.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.PlayGame;
            this.pbxGame.Location = new System.Drawing.Point(833, 243);
            this.pbxGame.Name = "pbxGame";
            this.pbxGame.Size = new System.Drawing.Size(238, 108);
            this.pbxGame.TabIndex = 3;
            this.pbxGame.TabStop = false;
            this.pbxGame.Click += new System.EventHandler(this.pbxGame_Click);
            // 
            // pbxBuild
            // 
            this.pbxBuild.BackColor = System.Drawing.Color.Transparent;
            this.pbxBuild.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Build;
            this.pbxBuild.Location = new System.Drawing.Point(833, 357);
            this.pbxBuild.Name = "pbxBuild";
            this.pbxBuild.Size = new System.Drawing.Size(238, 108);
            this.pbxBuild.TabIndex = 4;
            this.pbxBuild.TabStop = false;
            this.pbxBuild.Click += new System.EventHandler(this.pbxBuild_Click);
            // 
            // pbxHelp
            // 
            this.pbxHelp.BackColor = System.Drawing.Color.Transparent;
            this.pbxHelp.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Help;
            this.pbxHelp.Location = new System.Drawing.Point(833, 471);
            this.pbxHelp.Name = "pbxHelp";
            this.pbxHelp.Size = new System.Drawing.Size(238, 108);
            this.pbxHelp.TabIndex = 5;
            this.pbxHelp.TabStop = false;
            this.pbxHelp.Click += new System.EventHandler(this.pbxHelp_Click);
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Ghost_Pew_Pew_Game.Properties.Resources.HauntedHouse;
            this.ClientSize = new System.Drawing.Size(1264, 682);
            this.Controls.Add(this.pbxHelp);
            this.Controls.Add(this.pbxBuild);
            this.Controls.Add(this.pbxGame);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ghost Pew Pew - Menu";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMenu_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pbxGame)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxBuild)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHelp)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pbxGame;
        private System.Windows.Forms.PictureBox pbxBuild;
        private System.Windows.Forms.PictureBox pbxHelp;
    }
}