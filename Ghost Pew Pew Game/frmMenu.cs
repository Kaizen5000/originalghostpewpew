﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ghost_Pew_Pew_Game
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }
        private void pbxGame_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormHolder.Game.Show();
        }

        private void pbxBuild_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormHolder.Build.Show();
        }

        private void pbxHelp_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormHolder.Help.Show();
                
        }

        private void frmMenu_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
