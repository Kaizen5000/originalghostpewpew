﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ghost_Pew_Pew_Game
{
    public partial class frmBuild : Form
    {
        public frmBuild()
        {
            InitializeComponent();
        }
        #region Initialisation
        /*
         * This region is for the initialisation of the arrays that are used in this form 
         */ 
        int[] points = new int[] { Properties.Settings.Default.p1Points, Properties.Settings.Default.p2Points };
        //an integer array called Points. This is used to store the unused attribute points for both player one and two, with the indexes 0 and 1 respectively
        //the values for p1Points and p2Points are loaded from the settings file
        int[,] playerAttributes = new int[2, 4] { 
        { 
            Properties.Settings.Default.p1Damage,
            Properties.Settings.Default.p1Stamina,
            Properties.Settings.Default.p1Parry,
            Properties.Settings.Default.p1Jump,
        }, 
        { 
            Properties.Settings.Default.p2Damage,
            Properties.Settings.Default.p2Stamina,
            Properties.Settings.Default.p2Parry,
            Properties.Settings.Default.p2Jump,
        } };
        //playerAttributes stores the values that players have assigned to each attribute, this is also saved in the settings file
        bool finishClose = false; //bool for determining whether to exit the application or not when the form is closing
        #endregion
        #region Form Load
        private void frmBuild_Load(object sender, EventArgs e)
        {
            /*
             * This event handler is for the form loading, once all objects have been created, this code will be executed
             * This method is responsible for updating the progress bars to show the correct values and the labels to show the amount of unassigned points
             */
            UpdateProgressBar(playerAttributes[0, 0], pgbP1Damage); //Calls the UpdateProgressBar method while passing through the each element in the array with its respective progressbar
            UpdateProgressBar(playerAttributes[0, 1], pgbP1Stamina); //I couldn't use a loop very effectively for this because there are different progress bars that need to be passed through for each
            UpdateProgressBar(playerAttributes[0, 2], pgbP1Parry);
            UpdateProgressBar(playerAttributes[0, 3], pgbP1Jump);
            UpdateProgressBar(playerAttributes[1, 0], pgbP2Damage);
            UpdateProgressBar(playerAttributes[1, 1], pgbP2Stamina);
            UpdateProgressBar(playerAttributes[1, 2], pgbP2Parry);
            UpdateProgressBar(playerAttributes[1, 3], pgbP2Jump);
            lblP1Points.Text = "Points Remaining " + points[0]; //updates the lblP1Points label with some text and the value of unassigned points
            lblP2Points.Text = "Points Remaining " + points[1]; //same update, but for player 2
        }
        #endregion
        #region Change Points
        void ChangePoints(int playerIndex, bool isPlus, int index, ProgressBar pgbDisplay)
        {
            /*
             * This method is used to change the value for the attribute points when a button click's event handler calls this method
             * The inputs include the index of the player, a boolean of whether the plus or minus option is chosen, an integer for the index of the attribute in the array
             * and a respective progress bar that will be changed
             */
 
            if (isPlus == true) //checks if isPlus is true
            {
                if (points[playerIndex] > 0 && playerAttributes[playerIndex, index] < 4)//checks to make sure that there are an adequate number of points to assign and that the maximum of points for     
                {                                                                       //that attribute hasn't been reached
                    playerAttributes[playerIndex, index]++; //increment the attribute
                    points[playerIndex]--; //decrement the player's unused points value
                }
            }
            else //if it is a minus
            {
                if (playerAttributes[playerIndex, index] > 1) //checks that the minimum value for the attribute has not been reached
                {
                    playerAttributes[playerIndex, index]--; //decrement the attribute
                    points[playerIndex]++; //increment the player's unused points value
                }
            }
           
            switch (playerIndex) //tests playerIndex, which can be 0 for polayer 1 or 1 or player 2
            {
                case 0: //player 1
                    {
                        lblP1Points.Text = "Points Remaining " + points[playerIndex]; //updates the remaining points label for player 1
                    }
                    break;
                case 1: //player 2
                    {
                        lblP2Points.Text = "Points Remaining " + points[playerIndex]; //updates the remaining points label for player 2
                    }
                    break;
            }
            UpdateProgressBar(playerAttributes[playerIndex, index], pgbDisplay); //passes in the respective value and its progress bar as parameters into a method
            
        }
        #endregion
        #region Update Progress Bar
        void UpdateProgressBar(int attributeValue, ProgressBar progressBar)
        {
            /*
             * This method is responsible for changing the values on each progress bar to show its correct value
             * The inputs for this method include the value for an attribute and its respective progress bar
             */
            switch (attributeValue) // I use a case statement because a simple multiplication of the attributeValue couldn't give the values I wanted for the progress bar
            { //sets the value of the progress bar in each case
                case 1:
                    { progressBar.Value = 0; }
                    break;
                case 2:
                    { progressBar.Value = 33; }
                    break;
                case 3:
                    { progressBar.Value = 66; }
                    break;
                case 4:
                    { progressBar.Value = 100; }
                    break;
            }
        }
        #endregion
        private void frmBuild_FormClosing(object sender, FormClosingEventArgs e)
        {
            /*
             * Event handler is triggered when the form is closing
             */
            
            if (finishClose == false)
            {
                Application.Exit(); //closes entire application
            }
        }
        void UpdateSettings()
        {
            /*
             * This method updates each value in the settings file with its corresponding value in the playerAttributes array
             */ 
            Properties.Settings.Default.p1Damage = playerAttributes[0, 0];
            Properties.Settings.Default.p1Stamina = playerAttributes[0, 1];
            Properties.Settings.Default.p1Parry = playerAttributes[0, 2];
            Properties.Settings.Default.p1Jump = playerAttributes[0, 3];
            Properties.Settings.Default.p2Damage = playerAttributes[1, 0];
            Properties.Settings.Default.p2Stamina = playerAttributes[1, 1];
            Properties.Settings.Default.p2Parry = playerAttributes[1, 2];
            Properties.Settings.Default.p2Jump = playerAttributes[1, 3];
            Properties.Settings.Default.p1Points = points[0];
            Properties.Settings.Default.p2Points = points[1];
            Properties.Settings.Default.Save(); //calls the save method which makes changes persistent after the application exits
        }
        /*
         * "The Player One Picture Boxes" and "Player Two Picture Boxes" code are event handlers for picture box click events
         * They all call the ChangePoints method but pass through different parameters as each picture box that is clicked has a different effect from another
         * The parameters, in order, are: player index for the array, boolean to determine whether plus or minus has been clicked, index of the attribute that is being changed
         * and finally, the corresponding progress bar
         */ 
        #region Player One Picture Boxes
        #region Plus
        private void pbxP1DamagePlus_Click(object sender, EventArgs e)
        {
            ChangePoints(0, true, 0, pgbP1Damage);
        }
        private void pbxP1StaminaPlus_Click(object sender, EventArgs e)
        {
            ChangePoints(0, true, 1, pgbP1Stamina);
        }
        private void pbxP1ParryPlus_Click(object sender, EventArgs e)
        {
            ChangePoints(0, true, 2, pgbP1Parry);
        }
        private void pbxP1JumpPlus_Click(object sender, EventArgs e)
        {
            ChangePoints(0, true, 3, pgbP1Jump);
        }
        #endregion
        #region Minus
        private void pbxP1DamageMinus_Click(object sender, EventArgs e)
        {
            ChangePoints(0, false, 0, pgbP1Damage);
        }
        private void pbxP1StaminaMinus_Click(object sender, EventArgs e)
        {
            ChangePoints(0, false, 1, pgbP1Stamina);
        }
        private void pbxP1ParryMinus_Click(object sender, EventArgs e)
        {
            ChangePoints(0, false, 2, pgbP1Parry);
        }
        private void pbxP1JumpMinus_Click(object sender, EventArgs e)
        {
            ChangePoints(0, false, 3, pgbP1Jump);
        }
        #endregion
        #endregion
        #region Player Two Picture Boxes
        #region Plus
        private void pbxP2DamagePlus_Click(object sender, EventArgs e)
        {
            ChangePoints(1, true, 0, pgbP2Damage);
        }
        private void pbxP2StaminaPlus_Click(object sender, EventArgs e)
        {
            ChangePoints(1, true, 1, pgbP2Stamina);
        }

        private void pbxP2ParryPlus_Click(object sender, EventArgs e)
        {
            ChangePoints(1, true, 2, pgbP2Parry);
        }

        private void pbxP2JumpPlus_Click(object sender, EventArgs e)
        {
            ChangePoints(1, true, 3, pgbP2Jump);
        }
        #endregion
        #region Minus
        private void pbxP2DamageMinus_Click(object sender, EventArgs e)
        {
            ChangePoints(1, false, 0, pgbP2Damage);
        }
        private void pbxP2StaminaMinus_Click(object sender, EventArgs e)
        {
            ChangePoints(1, false, 1, pgbP2Stamina);
        }
        private void pbxP2ParryMinus_Click(object sender, EventArgs e)
        {
            ChangePoints(1, false, 2, pgbP2Parry);
        }
        private void pbxP2JumpMinus_Click(object sender, EventArgs e)
        {
            ChangePoints(1, false, 3, pgbP2Jump);
        }
        #endregion

        private void pgbP1Stamina_Click(object sender, EventArgs e)
        {

        }
        #endregion

        private void pbxGame_Click(object sender, EventArgs e)
        {
            UpdateSettings(); //calls UpdateSettings method
            FormHolder.Game.Show(); //shows the game form
            finishClose = true; //sets finishClose to true, this is so the application doesn't exit when the for closes
            this.Close();//closes current form
        }

        private void pbxBack_Click(object sender, EventArgs e)
        {
            UpdateSettings(); //calls UpdateSettings method
            FormHolder.Menu.Show(); //shows the menu form
            finishClose = true; //sets finishClose to true, this is so the application doesn't exit when the for closes
            this.Close(); //closes current form
            
        }
    }
}