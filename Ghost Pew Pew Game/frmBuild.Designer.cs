﻿namespace Ghost_Pew_Pew_Game
{
    partial class frmBuild
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBuild));
            this.pgbP1Damage = new System.Windows.Forms.ProgressBar();
            this.pgbP2Damage = new System.Windows.Forms.ProgressBar();
            this.pgbP1Jump = new System.Windows.Forms.ProgressBar();
            this.pgbP1Parry = new System.Windows.Forms.ProgressBar();
            this.pgbP1Stamina = new System.Windows.Forms.ProgressBar();
            this.pgbP2Jump = new System.Windows.Forms.ProgressBar();
            this.pgbP2Parry = new System.Windows.Forms.ProgressBar();
            this.pgbP2Stamina = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblP1Points = new System.Windows.Forms.Label();
            this.lblP2Points = new System.Windows.Forms.Label();
            this.pbxP2StaminaPlus = new System.Windows.Forms.PictureBox();
            this.pbxP2ParryPlus = new System.Windows.Forms.PictureBox();
            this.pbxP2JumpPlus = new System.Windows.Forms.PictureBox();
            this.pbxP2DamagePlus = new System.Windows.Forms.PictureBox();
            this.pbxP2StaminaMinus = new System.Windows.Forms.PictureBox();
            this.pbxP2ParryMinus = new System.Windows.Forms.PictureBox();
            this.pbxP2JumpMinus = new System.Windows.Forms.PictureBox();
            this.pbxP2DamageMinus = new System.Windows.Forms.PictureBox();
            this.pbxP1StaminaMinus = new System.Windows.Forms.PictureBox();
            this.pbxP1ParryMinus = new System.Windows.Forms.PictureBox();
            this.pbxP1JumpMinus = new System.Windows.Forms.PictureBox();
            this.pbxP1StaminaPlus = new System.Windows.Forms.PictureBox();
            this.pbxP1ParryPlus = new System.Windows.Forms.PictureBox();
            this.pbxP1JumpPlus = new System.Windows.Forms.PictureBox();
            this.pbxP1DamagePlus = new System.Windows.Forms.PictureBox();
            this.pbxP1DamageMinus = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pbxP1 = new System.Windows.Forms.PictureBox();
            this.pbxGame = new System.Windows.Forms.PictureBox();
            this.pbxBack = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP2StaminaPlus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP2ParryPlus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP2JumpPlus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP2DamagePlus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP2StaminaMinus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP2ParryMinus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP2JumpMinus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP2DamageMinus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP1StaminaMinus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP1ParryMinus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP1JumpMinus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP1StaminaPlus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP1ParryPlus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP1JumpPlus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP1DamagePlus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP1DamageMinus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxGame)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxBack)).BeginInit();
            this.SuspendLayout();
            // 
            // pgbP1Damage
            // 
            this.pgbP1Damage.BackColor = System.Drawing.Color.Black;
            this.pgbP1Damage.ForeColor = System.Drawing.Color.Red;
            this.pgbP1Damage.Location = new System.Drawing.Point(246, 329);
            this.pgbP1Damage.Name = "pgbP1Damage";
            this.pgbP1Damage.Size = new System.Drawing.Size(231, 38);
            this.pgbP1Damage.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pgbP1Damage.TabIndex = 3;
            // 
            // pgbP2Damage
            // 
            this.pgbP2Damage.BackColor = System.Drawing.Color.Black;
            this.pgbP2Damage.ForeColor = System.Drawing.Color.Red;
            this.pgbP2Damage.Location = new System.Drawing.Point(785, 329);
            this.pgbP2Damage.Name = "pgbP2Damage";
            this.pgbP2Damage.Size = new System.Drawing.Size(231, 38);
            this.pgbP2Damage.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pgbP2Damage.TabIndex = 4;
            // 
            // pgbP1Jump
            // 
            this.pgbP1Jump.BackColor = System.Drawing.Color.Black;
            this.pgbP1Jump.ForeColor = System.Drawing.Color.Red;
            this.pgbP1Jump.Location = new System.Drawing.Point(246, 564);
            this.pgbP1Jump.Name = "pgbP1Jump";
            this.pgbP1Jump.Size = new System.Drawing.Size(231, 38);
            this.pgbP1Jump.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pgbP1Jump.TabIndex = 5;
            // 
            // pgbP1Parry
            // 
            this.pgbP1Parry.BackColor = System.Drawing.Color.Black;
            this.pgbP1Parry.ForeColor = System.Drawing.Color.Red;
            this.pgbP1Parry.Location = new System.Drawing.Point(246, 482);
            this.pgbP1Parry.Name = "pgbP1Parry";
            this.pgbP1Parry.Size = new System.Drawing.Size(231, 38);
            this.pgbP1Parry.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pgbP1Parry.TabIndex = 6;
            // 
            // pgbP1Stamina
            // 
            this.pgbP1Stamina.BackColor = System.Drawing.Color.Black;
            this.pgbP1Stamina.ForeColor = System.Drawing.Color.Red;
            this.pgbP1Stamina.Location = new System.Drawing.Point(246, 403);
            this.pgbP1Stamina.Name = "pgbP1Stamina";
            this.pgbP1Stamina.Size = new System.Drawing.Size(231, 38);
            this.pgbP1Stamina.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pgbP1Stamina.TabIndex = 7;
            this.pgbP1Stamina.Click += new System.EventHandler(this.pgbP1Stamina_Click);
            // 
            // pgbP2Jump
            // 
            this.pgbP2Jump.BackColor = System.Drawing.Color.Black;
            this.pgbP2Jump.ForeColor = System.Drawing.Color.Red;
            this.pgbP2Jump.Location = new System.Drawing.Point(785, 564);
            this.pgbP2Jump.Name = "pgbP2Jump";
            this.pgbP2Jump.Size = new System.Drawing.Size(231, 35);
            this.pgbP2Jump.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pgbP2Jump.TabIndex = 8;
            // 
            // pgbP2Parry
            // 
            this.pgbP2Parry.BackColor = System.Drawing.Color.Black;
            this.pgbP2Parry.ForeColor = System.Drawing.Color.Red;
            this.pgbP2Parry.Location = new System.Drawing.Point(785, 482);
            this.pgbP2Parry.Name = "pgbP2Parry";
            this.pgbP2Parry.Size = new System.Drawing.Size(231, 38);
            this.pgbP2Parry.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pgbP2Parry.TabIndex = 9;
            // 
            // pgbP2Stamina
            // 
            this.pgbP2Stamina.BackColor = System.Drawing.Color.Black;
            this.pgbP2Stamina.ForeColor = System.Drawing.Color.Red;
            this.pgbP2Stamina.Location = new System.Drawing.Point(785, 403);
            this.pgbP2Stamina.Name = "pgbP2Stamina";
            this.pgbP2Stamina.Size = new System.Drawing.Size(231, 38);
            this.pgbP2Stamina.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.pgbP2Stamina.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(241, 301);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 25);
            this.label1.TabIndex = 11;
            this.label1.Text = "Damage";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(780, 301);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 25);
            this.label2.TabIndex = 12;
            this.label2.Text = "Damage";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(241, 375);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 25);
            this.label3.TabIndex = 13;
            this.label3.Text = "Stamina";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(241, 536);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 25);
            this.label4.TabIndex = 14;
            this.label4.Text = "Jump";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(241, 454);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 25);
            this.label5.TabIndex = 15;
            this.label5.Text = "Parry";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(780, 454);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 25);
            this.label6.TabIndex = 22;
            this.label6.Text = "Parry";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(780, 536);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 25);
            this.label7.TabIndex = 21;
            this.label7.Text = "Jump";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(780, 375);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(97, 25);
            this.label8.TabIndex = 20;
            this.label8.Text = "Stamina";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(294, 105);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(99, 25);
            this.label9.TabIndex = 35;
            this.label9.Text = "Player 1";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(844, 105);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(99, 25);
            this.label10.TabIndex = 36;
            this.label10.Text = "Player 2";
            // 
            // lblP1Points
            // 
            this.lblP1Points.AutoSize = true;
            this.lblP1Points.BackColor = System.Drawing.Color.Transparent;
            this.lblP1Points.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblP1Points.ForeColor = System.Drawing.Color.White;
            this.lblP1Points.Location = new System.Drawing.Point(210, 620);
            this.lblP1Points.Name = "lblP1Points";
            this.lblP1Points.Size = new System.Drawing.Size(203, 25);
            this.lblP1Points.TabIndex = 52;
            this.lblP1Points.Text = "Points Remaining:";
            // 
            // lblP2Points
            // 
            this.lblP2Points.AutoSize = true;
            this.lblP2Points.BackColor = System.Drawing.Color.Transparent;
            this.lblP2Points.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblP2Points.ForeColor = System.Drawing.Color.White;
            this.lblP2Points.Location = new System.Drawing.Point(760, 620);
            this.lblP2Points.Name = "lblP2Points";
            this.lblP2Points.Size = new System.Drawing.Size(203, 25);
            this.lblP2Points.TabIndex = 53;
            this.lblP2Points.Text = "Points Remaining:";
            // 
            // pbxP2StaminaPlus
            // 
            this.pbxP2StaminaPlus.BackColor = System.Drawing.Color.Transparent;
            this.pbxP2StaminaPlus.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Plus;
            this.pbxP2StaminaPlus.Location = new System.Drawing.Point(1022, 403);
            this.pbxP2StaminaPlus.Name = "pbxP2StaminaPlus";
            this.pbxP2StaminaPlus.Size = new System.Drawing.Size(40, 38);
            this.pbxP2StaminaPlus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxP2StaminaPlus.TabIndex = 51;
            this.pbxP2StaminaPlus.TabStop = false;
            this.pbxP2StaminaPlus.Click += new System.EventHandler(this.pbxP2StaminaPlus_Click);
            // 
            // pbxP2ParryPlus
            // 
            this.pbxP2ParryPlus.BackColor = System.Drawing.Color.Transparent;
            this.pbxP2ParryPlus.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Plus;
            this.pbxP2ParryPlus.Location = new System.Drawing.Point(1022, 482);
            this.pbxP2ParryPlus.Name = "pbxP2ParryPlus";
            this.pbxP2ParryPlus.Size = new System.Drawing.Size(40, 38);
            this.pbxP2ParryPlus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxP2ParryPlus.TabIndex = 50;
            this.pbxP2ParryPlus.TabStop = false;
            this.pbxP2ParryPlus.Click += new System.EventHandler(this.pbxP2ParryPlus_Click);
            // 
            // pbxP2JumpPlus
            // 
            this.pbxP2JumpPlus.BackColor = System.Drawing.Color.Transparent;
            this.pbxP2JumpPlus.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Plus;
            this.pbxP2JumpPlus.Location = new System.Drawing.Point(1022, 564);
            this.pbxP2JumpPlus.Name = "pbxP2JumpPlus";
            this.pbxP2JumpPlus.Size = new System.Drawing.Size(40, 38);
            this.pbxP2JumpPlus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxP2JumpPlus.TabIndex = 49;
            this.pbxP2JumpPlus.TabStop = false;
            this.pbxP2JumpPlus.Click += new System.EventHandler(this.pbxP2JumpPlus_Click);
            // 
            // pbxP2DamagePlus
            // 
            this.pbxP2DamagePlus.BackColor = System.Drawing.Color.Transparent;
            this.pbxP2DamagePlus.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Plus;
            this.pbxP2DamagePlus.Location = new System.Drawing.Point(1022, 329);
            this.pbxP2DamagePlus.Name = "pbxP2DamagePlus";
            this.pbxP2DamagePlus.Size = new System.Drawing.Size(40, 38);
            this.pbxP2DamagePlus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxP2DamagePlus.TabIndex = 48;
            this.pbxP2DamagePlus.TabStop = false;
            this.pbxP2DamagePlus.Click += new System.EventHandler(this.pbxP2DamagePlus_Click);
            // 
            // pbxP2StaminaMinus
            // 
            this.pbxP2StaminaMinus.BackColor = System.Drawing.Color.Transparent;
            this.pbxP2StaminaMinus.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Minus;
            this.pbxP2StaminaMinus.Location = new System.Drawing.Point(741, 403);
            this.pbxP2StaminaMinus.Name = "pbxP2StaminaMinus";
            this.pbxP2StaminaMinus.Size = new System.Drawing.Size(38, 38);
            this.pbxP2StaminaMinus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxP2StaminaMinus.TabIndex = 47;
            this.pbxP2StaminaMinus.TabStop = false;
            this.pbxP2StaminaMinus.Click += new System.EventHandler(this.pbxP2StaminaMinus_Click);
            // 
            // pbxP2ParryMinus
            // 
            this.pbxP2ParryMinus.BackColor = System.Drawing.Color.Transparent;
            this.pbxP2ParryMinus.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Minus;
            this.pbxP2ParryMinus.Location = new System.Drawing.Point(741, 482);
            this.pbxP2ParryMinus.Name = "pbxP2ParryMinus";
            this.pbxP2ParryMinus.Size = new System.Drawing.Size(38, 38);
            this.pbxP2ParryMinus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxP2ParryMinus.TabIndex = 46;
            this.pbxP2ParryMinus.TabStop = false;
            this.pbxP2ParryMinus.Click += new System.EventHandler(this.pbxP2ParryMinus_Click);
            // 
            // pbxP2JumpMinus
            // 
            this.pbxP2JumpMinus.BackColor = System.Drawing.Color.Transparent;
            this.pbxP2JumpMinus.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Minus;
            this.pbxP2JumpMinus.Location = new System.Drawing.Point(741, 564);
            this.pbxP2JumpMinus.Name = "pbxP2JumpMinus";
            this.pbxP2JumpMinus.Size = new System.Drawing.Size(38, 38);
            this.pbxP2JumpMinus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxP2JumpMinus.TabIndex = 45;
            this.pbxP2JumpMinus.TabStop = false;
            this.pbxP2JumpMinus.Click += new System.EventHandler(this.pbxP2JumpMinus_Click);
            // 
            // pbxP2DamageMinus
            // 
            this.pbxP2DamageMinus.BackColor = System.Drawing.Color.Transparent;
            this.pbxP2DamageMinus.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Minus;
            this.pbxP2DamageMinus.Location = new System.Drawing.Point(741, 329);
            this.pbxP2DamageMinus.Name = "pbxP2DamageMinus";
            this.pbxP2DamageMinus.Size = new System.Drawing.Size(38, 38);
            this.pbxP2DamageMinus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxP2DamageMinus.TabIndex = 44;
            this.pbxP2DamageMinus.TabStop = false;
            this.pbxP2DamageMinus.Click += new System.EventHandler(this.pbxP2DamageMinus_Click);
            // 
            // pbxP1StaminaMinus
            // 
            this.pbxP1StaminaMinus.BackColor = System.Drawing.Color.Transparent;
            this.pbxP1StaminaMinus.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Minus;
            this.pbxP1StaminaMinus.Location = new System.Drawing.Point(202, 403);
            this.pbxP1StaminaMinus.Name = "pbxP1StaminaMinus";
            this.pbxP1StaminaMinus.Size = new System.Drawing.Size(38, 38);
            this.pbxP1StaminaMinus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxP1StaminaMinus.TabIndex = 43;
            this.pbxP1StaminaMinus.TabStop = false;
            this.pbxP1StaminaMinus.Click += new System.EventHandler(this.pbxP1StaminaMinus_Click);
            // 
            // pbxP1ParryMinus
            // 
            this.pbxP1ParryMinus.BackColor = System.Drawing.Color.Transparent;
            this.pbxP1ParryMinus.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Minus;
            this.pbxP1ParryMinus.Location = new System.Drawing.Point(202, 482);
            this.pbxP1ParryMinus.Name = "pbxP1ParryMinus";
            this.pbxP1ParryMinus.Size = new System.Drawing.Size(38, 38);
            this.pbxP1ParryMinus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxP1ParryMinus.TabIndex = 42;
            this.pbxP1ParryMinus.TabStop = false;
            this.pbxP1ParryMinus.Click += new System.EventHandler(this.pbxP1ParryMinus_Click);
            // 
            // pbxP1JumpMinus
            // 
            this.pbxP1JumpMinus.BackColor = System.Drawing.Color.Transparent;
            this.pbxP1JumpMinus.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Minus;
            this.pbxP1JumpMinus.Location = new System.Drawing.Point(202, 564);
            this.pbxP1JumpMinus.Name = "pbxP1JumpMinus";
            this.pbxP1JumpMinus.Size = new System.Drawing.Size(38, 38);
            this.pbxP1JumpMinus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxP1JumpMinus.TabIndex = 41;
            this.pbxP1JumpMinus.TabStop = false;
            this.pbxP1JumpMinus.Click += new System.EventHandler(this.pbxP1JumpMinus_Click);
            // 
            // pbxP1StaminaPlus
            // 
            this.pbxP1StaminaPlus.BackColor = System.Drawing.Color.Transparent;
            this.pbxP1StaminaPlus.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Plus;
            this.pbxP1StaminaPlus.Location = new System.Drawing.Point(483, 403);
            this.pbxP1StaminaPlus.Name = "pbxP1StaminaPlus";
            this.pbxP1StaminaPlus.Size = new System.Drawing.Size(40, 38);
            this.pbxP1StaminaPlus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxP1StaminaPlus.TabIndex = 40;
            this.pbxP1StaminaPlus.TabStop = false;
            this.pbxP1StaminaPlus.Click += new System.EventHandler(this.pbxP1StaminaPlus_Click);
            // 
            // pbxP1ParryPlus
            // 
            this.pbxP1ParryPlus.BackColor = System.Drawing.Color.Transparent;
            this.pbxP1ParryPlus.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Plus;
            this.pbxP1ParryPlus.Location = new System.Drawing.Point(483, 482);
            this.pbxP1ParryPlus.Name = "pbxP1ParryPlus";
            this.pbxP1ParryPlus.Size = new System.Drawing.Size(40, 38);
            this.pbxP1ParryPlus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxP1ParryPlus.TabIndex = 39;
            this.pbxP1ParryPlus.TabStop = false;
            this.pbxP1ParryPlus.Click += new System.EventHandler(this.pbxP1ParryPlus_Click);
            // 
            // pbxP1JumpPlus
            // 
            this.pbxP1JumpPlus.BackColor = System.Drawing.Color.Transparent;
            this.pbxP1JumpPlus.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Plus;
            this.pbxP1JumpPlus.Location = new System.Drawing.Point(483, 564);
            this.pbxP1JumpPlus.Name = "pbxP1JumpPlus";
            this.pbxP1JumpPlus.Size = new System.Drawing.Size(40, 38);
            this.pbxP1JumpPlus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxP1JumpPlus.TabIndex = 38;
            this.pbxP1JumpPlus.TabStop = false;
            this.pbxP1JumpPlus.Click += new System.EventHandler(this.pbxP1JumpPlus_Click);
            // 
            // pbxP1DamagePlus
            // 
            this.pbxP1DamagePlus.BackColor = System.Drawing.Color.Transparent;
            this.pbxP1DamagePlus.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Plus;
            this.pbxP1DamagePlus.Location = new System.Drawing.Point(483, 329);
            this.pbxP1DamagePlus.Name = "pbxP1DamagePlus";
            this.pbxP1DamagePlus.Size = new System.Drawing.Size(40, 38);
            this.pbxP1DamagePlus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxP1DamagePlus.TabIndex = 37;
            this.pbxP1DamagePlus.TabStop = false;
            this.pbxP1DamagePlus.Click += new System.EventHandler(this.pbxP1DamagePlus_Click);
            // 
            // pbxP1DamageMinus
            // 
            this.pbxP1DamageMinus.BackColor = System.Drawing.Color.Transparent;
            this.pbxP1DamageMinus.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Minus;
            this.pbxP1DamageMinus.Location = new System.Drawing.Point(202, 329);
            this.pbxP1DamageMinus.Name = "pbxP1DamageMinus";
            this.pbxP1DamageMinus.Size = new System.Drawing.Size(38, 38);
            this.pbxP1DamageMinus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxP1DamageMinus.TabIndex = 16;
            this.pbxP1DamageMinus.TabStop = false;
            this.pbxP1DamageMinus.Click += new System.EventHandler(this.pbxP1DamageMinus_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.GhostIdleLeft1;
            this.pictureBox1.Location = new System.Drawing.Point(849, 133);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(114, 143);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // pbxP1
            // 
            this.pbxP1.BackColor = System.Drawing.Color.Transparent;
            this.pbxP1.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.GhostIdleRight1;
            this.pbxP1.Location = new System.Drawing.Point(299, 133);
            this.pbxP1.Name = "pbxP1";
            this.pbxP1.Size = new System.Drawing.Size(114, 143);
            this.pbxP1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxP1.TabIndex = 1;
            this.pbxP1.TabStop = false;
            // 
            // pbxGame
            // 
            this.pbxGame.BackColor = System.Drawing.Color.Transparent;
            this.pbxGame.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.PlayGame;
            this.pbxGame.Location = new System.Drawing.Point(506, 191);
            this.pbxGame.Name = "pbxGame";
            this.pbxGame.Size = new System.Drawing.Size(238, 108);
            this.pbxGame.TabIndex = 54;
            this.pbxGame.TabStop = false;
            this.pbxGame.Click += new System.EventHandler(this.pbxGame_Click);
            // 
            // pbxBack
            // 
            this.pbxBack.BackColor = System.Drawing.Color.Transparent;
            this.pbxBack.Image = global::Ghost_Pew_Pew_Game.Properties.Resources.Back;
            this.pbxBack.Location = new System.Drawing.Point(6, 574);
            this.pbxBack.Name = "pbxBack";
            this.pbxBack.Size = new System.Drawing.Size(190, 96);
            this.pbxBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxBack.TabIndex = 55;
            this.pbxBack.TabStop = false;
            this.pbxBack.Click += new System.EventHandler(this.pbxBack_Click);
            // 
            // frmBuild
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.BackgroundImage = global::Ghost_Pew_Pew_Game.Properties.Resources.HauntedForest;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1264, 682);
            this.Controls.Add(this.pbxBack);
            this.Controls.Add(this.pbxGame);
            this.Controls.Add(this.lblP2Points);
            this.Controls.Add(this.lblP1Points);
            this.Controls.Add(this.pbxP2StaminaPlus);
            this.Controls.Add(this.pbxP2ParryPlus);
            this.Controls.Add(this.pbxP2JumpPlus);
            this.Controls.Add(this.pbxP2DamagePlus);
            this.Controls.Add(this.pbxP2StaminaMinus);
            this.Controls.Add(this.pbxP2ParryMinus);
            this.Controls.Add(this.pbxP2JumpMinus);
            this.Controls.Add(this.pbxP2DamageMinus);
            this.Controls.Add(this.pbxP1StaminaMinus);
            this.Controls.Add(this.pbxP1ParryMinus);
            this.Controls.Add(this.pbxP1JumpMinus);
            this.Controls.Add(this.pbxP1StaminaPlus);
            this.Controls.Add(this.pbxP1ParryPlus);
            this.Controls.Add(this.pbxP1JumpPlus);
            this.Controls.Add(this.pbxP1DamagePlus);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.pbxP1DamageMinus);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pgbP2Stamina);
            this.Controls.Add(this.pgbP2Parry);
            this.Controls.Add(this.pgbP2Jump);
            this.Controls.Add(this.pgbP1Stamina);
            this.Controls.Add(this.pgbP1Parry);
            this.Controls.Add(this.pgbP1Jump);
            this.Controls.Add(this.pgbP2Damage);
            this.Controls.Add(this.pgbP1Damage);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pbxP1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmBuild";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Ghost Pew Pew - Build";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmBuild_FormClosing);
            this.Load += new System.EventHandler(this.frmBuild_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbxP2StaminaPlus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP2ParryPlus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP2JumpPlus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP2DamagePlus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP2StaminaMinus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP2ParryMinus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP2JumpMinus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP2DamageMinus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP1StaminaMinus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP1ParryMinus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP1JumpMinus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP1StaminaPlus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP1ParryPlus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP1JumpPlus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP1DamagePlus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP1DamageMinus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxP1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxGame)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxBack)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbxP1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ProgressBar pgbP1Damage;
        private System.Windows.Forms.ProgressBar pgbP2Damage;
        private System.Windows.Forms.ProgressBar pgbP1Jump;
        private System.Windows.Forms.ProgressBar pgbP1Parry;
        private System.Windows.Forms.ProgressBar pgbP1Stamina;
        private System.Windows.Forms.ProgressBar pgbP2Jump;
        private System.Windows.Forms.ProgressBar pgbP2Parry;
        private System.Windows.Forms.ProgressBar pgbP2Stamina;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pbxP1DamageMinus;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox pbxP1DamagePlus;
        private System.Windows.Forms.PictureBox pbxP1JumpPlus;
        private System.Windows.Forms.PictureBox pbxP1ParryPlus;
        private System.Windows.Forms.PictureBox pbxP1StaminaPlus;
        private System.Windows.Forms.PictureBox pbxP1JumpMinus;
        private System.Windows.Forms.PictureBox pbxP1ParryMinus;
        private System.Windows.Forms.PictureBox pbxP1StaminaMinus;
        private System.Windows.Forms.PictureBox pbxP2StaminaMinus;
        private System.Windows.Forms.PictureBox pbxP2ParryMinus;
        private System.Windows.Forms.PictureBox pbxP2JumpMinus;
        private System.Windows.Forms.PictureBox pbxP2DamageMinus;
        private System.Windows.Forms.PictureBox pbxP2StaminaPlus;
        private System.Windows.Forms.PictureBox pbxP2ParryPlus;
        private System.Windows.Forms.PictureBox pbxP2JumpPlus;
        private System.Windows.Forms.PictureBox pbxP2DamagePlus;
        private System.Windows.Forms.Label lblP1Points;
        private System.Windows.Forms.Label lblP2Points;
        private System.Windows.Forms.PictureBox pbxGame;
        private System.Windows.Forms.PictureBox pbxBack;
    }
}