﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ghost_Pew_Pew_Game
{
    public partial class frmHelp : Form
    {
        public frmHelp()
        {
            InitializeComponent();
        }
        private void tabPage1_Click(object sender, EventArgs e)
        {

        }
        bool finishClose = false;
        private void pbxBack_Click(object sender, EventArgs e)
        {
            FormHolder.Menu.Show();
            finishClose = true;
            this.Close();
            
        }

        private void frmHelp_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (finishClose == false)
            {
                Application.Exit();
            }
        }
    }
}
