﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Ghost_Pew_Pew_Game
{
    class FormHolder
    {
        /*
         * The purpose of this class is to open forms when called the static methods within this class are called.
         */ 
        //private so these can't be accessed outisde of the class
        //static methods need static variables
        static frmBuild build = new frmBuild(); //variables/fields that are used for their specified forms
        static frmGame game = new frmGame();
        static frmMenu menu = new frmMenu();
        static frmHelp help = new frmHelp();

        //public so it can be called from other forms
        //static so we don't need an instance of this class to use the methods
        public static frmMenu Menu
        {
            get
            {
                return menu; //returns menu to the code is calling this property
            }
        }
        public static frmBuild Build
        {
            get
            {
                build.Close(); //closes any open instances
                build = new frmBuild(); //creates a new form
                return build; //returns the form variable to the call
            }
        }
        public static frmGame Game
        {
            get
            {
                game.Close(); //closes any open instances
                game = new frmGame(); //creates a new form
                return game; //returns the form variable to the call
            }
        }
        public static frmHelp Help
        {
            get
            {
                help.Close(); //closes any open instances
                help = new frmHelp(); //creates a new form
                return help; //returns the form variable to the call
            }
        }
    }
}
