﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Ghost_Pew_Pew_Game
{
    public partial class frmGame : Form
    {
        public frmGame()
        {
            InitializeComponent(); // Creates objects for designer support.
            tmrMovement.Start(); // Starts tmrMovement timer, this is so movement is possible.
            tmrAnimation.Start(); // Starts tmrAnimation timer, this is so animation starts at the beginnning.
            backgroundImage = FormatBitmap(Properties.Resources.HauntedInterior);  // Passes Haunted Interior image into the FormatBitmap method and assigns the returned bitmap to backgroundImage variable.
                // this is used for the background
        }
        #region Declaration
        int[] animationState = new int[] { 0, 0 }; // Integer array used to store the number corresponding to the frame of the animation that is currently being used.
        // Stores the animationState value for both players

        int[] ghostState = new int[] { 0, 1 }; // Integer array used to hold an integer representing the animation that is going to be loaded into the respective picture box
            // Holds the ghostState for both players. These integers are used as the first index in the animation 2D array.

        int[,] playerAttributes = new int[2, 4] { 
        { 
            Properties.Settings.Default.p1Damage,
            Properties.Settings.Default.p1Stamina,
            Properties.Settings.Default.p1Parry,
            Properties.Settings.Default.p1Jump,
        }, 
        { 
            Properties.Settings.Default.p2Damage,
            Properties.Settings.Default.p2Stamina,
            Properties.Settings.Default.p2Parry,
            Properties.Settings.Default.p2Jump,
        } }; // This array of integers is used to hold the values assigned in the frmBuild form, these are used as multipliers to values in the game.
        bool p1Up, p1Left, p1Right, p2Up, p2Left, p2Right; // Bools for determining whether a button is held.
        int p1Force, p2Force; // Multipliers for the amount the player rises, this is decreased to slow the players' descent.
        bool p1LeftDirection, p2LeftDirection = true; // bools to determine which way the player is facing.
        int p1CurrentJump, p2CurrentJump; // Integers for how many jumps the player has left.
        bool p1Block, p2Block; // Bools to determine whether the player is blocking.
        bool endGame = false; // Bool to determine if the match is finished or not, this is used to stop multiple win notifications from triggering.
        int introCountdown = 3; // Integer to be used as a countdown before the game begins.
        int gameTime = 99; // Game time integer which is used as a time limit before the match is forcibly finished.
        bool finishClose = false; // Bool to switch between closing the form back to the main menu or exiting the application.
        Bitmap backgroundImage; // Bitmap variable to store the image used for the background.
        Bitmap[][] animation = new Bitmap[][] // Bitmap jagged array to hold the animation frames, a jagged array is used so there isn't a set amount of elements for each index.
        {
            /*
             * All images are obtained from the properties.resources location and are passed through the FormatBitmap method to reformat them to a format that is faster to load.
             * This is essential for a smoother animation.
             */
 
            // Ghost idle animation frames, facing left.
            new Bitmap[]{
                  FormatBitmap(Properties.Resources.GhostIdleLeft0),
                  FormatBitmap(Properties.Resources.GhostIdleLeft1),
                  FormatBitmap(Properties.Resources.GhostIdleLeft2)
              },

              // Ghost idle animation frames, facing right.
             new Bitmap[]{
                   FormatBitmap(Properties.Resources.GhostIdleRight0),
                   FormatBitmap(Properties.Resources.GhostIdleRight1),
                   FormatBitmap(Properties.Resources.GhostIdleRight2)
               },

               // Ghost blocking frames, for facing left and right.
             new Bitmap[]{
                    FormatBitmap(Properties.Resources.GhostBlockLeft),
                    FormatBitmap(Properties.Resources.GhostBlockRight),
             },

             // Fireball frames, facing left.
               new Bitmap[]{
                    FormatBitmap(Properties.Resources.FireballLeft0),
                    FormatBitmap(Properties.Resources.FireballLeft1),
                    FormatBitmap(Properties.Resources.FireballLeft2),
                    FormatBitmap(Properties.Resources.FireballLeft3),
                    FormatBitmap(Properties.Resources.FireballLeft4),
                    FormatBitmap(Properties.Resources.FireballLeft5),
                    FormatBitmap(Properties.Resources.FireballLeft6),
                    FormatBitmap(Properties.Resources.FireballLeft7),
                    FormatBitmap(Properties.Resources.FireballLeft8),
               },

               // Fireball frames, facing right.
                 new Bitmap[]{
                     FormatBitmap(Properties.Resources.FireballRight0),
                     FormatBitmap(Properties.Resources.FireballRight1),
                     FormatBitmap(Properties.Resources.FireballRight2),
                     FormatBitmap(Properties.Resources.FireballRight3),
                     FormatBitmap(Properties.Resources.FireballRight4),
                     FormatBitmap(Properties.Resources.FireballRight5),
                     FormatBitmap(Properties.Resources.FireballRight6),
                     FormatBitmap(Properties.Resources.FireballRight7),
                     FormatBitmap(Properties.Resources.FireballRight8),
                 },

                 // Ice shard frames, facing left.
                 new Bitmap[]
                 {
                     FormatBitmap(Properties.Resources.IceLeftTop),
                     FormatBitmap(Properties.Resources.IceLeftMiddle),
                     FormatBitmap(Properties.Resources.IceLeftBottom),
                 },

                 // Ice shard frames, facing right.
                 new Bitmap[]
                 {
                     FormatBitmap(Properties.Resources.IceRightTop),
                     FormatBitmap(Properties.Resources.IceRightMiddle),
                     FormatBitmap(Properties.Resources.IceRightBottom),
                 },

                 // Ghost parry frames, facing left.
                 new Bitmap[]
                 {
                     FormatBitmap(Properties.Resources.GhostParryLeft0),
                     FormatBitmap(Properties.Resources.GhostParryLeft1),
                     FormatBitmap(Properties.Resources.GhostParryLeft2),
                     FormatBitmap(Properties.Resources.GhostParryLeft3),
                     FormatBitmap(Properties.Resources.GhostParryLeft4),
                 },

                 // Ghost parry frames, facing right.
                 new Bitmap[]
                 {
                     FormatBitmap(Properties.Resources.GhostParryRight0),
                     FormatBitmap(Properties.Resources.GhostParryRight1),
                     FormatBitmap(Properties.Resources.GhostParryRight2),
                     FormatBitmap(Properties.Resources.GhostParryRight3),
                     FormatBitmap(Properties.Resources.GhostParryRight4),
                 }

        };
        #endregion
        #region Form Load
        private void frmGame_Load(object sender, EventArgs e)
        {
            lblGameTime.Parent = pbxBarHolder; // Parents the label lblGameTime to the picture box pbxBarHolder, this allows for the transparency in the label to show
                // contents of that picture box rather than the background image of the form.

            pgbP1Health.Value = 100; // Sets the value of the progress bar to the maximum value of 100.
            pgbP2Health.Value = 100; // Sets the value of the progress bar to the maximum value of 100.
            pgbP1Stamina.Value = 100; // Sets the value of the progress bar to the maximum value of 100.
            pgbP2Stamina.Value = 100; // Sets the value of the progress bar to the maximum value of 100.
            tmrStamina.Start(); // Starts the timer tmrStamina used to regenerate the stamina of both players; this refills the respective progress bars.

            tmrP1Parry.Interval = 150 * playerAttributes[0, 2]; // Sets the interval of player one's parry timer to 150 times the corresponding player attribute value.
            tmrP2Parry.Interval = 150 * playerAttributes[1, 2]; // Sets the interval of player two's parry timer to 150 times the corresponding player attribute value.
                // These timers are used as the parry window, while these timers are active, the corresponding player will be able to parry projectiles.

            tmrP1StaminaCooldown.Interval = 4000 - (500 * playerAttributes[0, 1]); // Sets the interval of the cooldown timer to a value affected by the corresponding player attributes stat.
            tmrP2StaminaCooldown.Interval = 4000 - (500 * playerAttributes[1, 1]); // Sets the interval of the cooldown timer to a value affected by the corresponding player attributes stat.
                // These timers are used to time the cooldown that players wil have to endure if they entirely deplete their stamina, the higher the player attribute point, the lower the interval.

            tmrIntro.Start(); // Starts the Intro timer, this timer counts down before the game begins.
        }
        #endregion
        #region Format Bitmap
        private static Bitmap FormatBitmap(Image sourceImage)
        {
            /*
             * This method reformats the image that is received as an input to a 32bppPArgb format which is a pixel format that allows for much faster displaying.
             */ 
            Bitmap output = new Bitmap(sourceImage.Width, sourceImage.Height, System.Drawing.Imaging.PixelFormat.Format32bppPArgb); // Creates a new bitmap with the same width and height
                // and uses the 32bppPArgb pixel format.
            using (Graphics gr = Graphics.FromImage(output)) // Creates and uses a new graphics object to be used for drawing.
            {
                gr.DrawImage(sourceImage, 0, 0, sourceImage.Width, sourceImage.Height); // Draws the image onto the output bitmap.
            }
            return output; // Returns the bitmap to the caller.
        }
        #endregion
        #region Paint
        private void frmGame_Paint(object sender, PaintEventArgs e)
        {
            /*
             * This paint event handler is called whenever the control (the form in this case) is drawn.
             * This code draws the background image on.
             */ 
            e.Graphics.DrawImage(backgroundImage, 0, -22, 1280, 720); // Paints the background image onto the game form, this is done at the y coordinate of -22 as the image is 22 pixels larger
                // than the form and I wanted to have the bottom 22 pixels in view.
        }
        #endregion
        #region tmrMovement
        private void tmrMovement_Tick(object sender, EventArgs e)
        {
            /*
             * This timer controls the movement of the players' picture boxes according to keyboard inputs by players.
             */
            OverlapChecker(); // Calls the Overlap Checker Method before moving anything. This method switches certain bools to false so that certain inputs are not received.
                // This is done so that ghosts cannot move further into each other, thereby preventing overlap.

            if (p1Up == true) // Checks if p1Up bool is true. If p1Up is true the player is jumping.
            {
                if (p1Force > 0) // Checks that p1Force is larger than 0. If so, the picture box is still rising.
                {
                    pbxP1.Top -= p1Force * 7; // Raises the player's picture box but 7 multiplied by p1Force.
                    p1Force--; // Decrement p1Force
                }
                else // If force is not larger than 0, the player should begin to fall.
                {
                    p1Up = false; // Sets p1Up to false.
                }
            }
            else // If p1Up is false, the player should be falling.
            {
                if (pbxP1.Top < 500) // If the player has not reached the ground yet.
                {
                    if (pbxP1.Left + pbxP1.Width > pbxP2.Left && pbxP1.Top + pbxP1.Height +7> pbxP2.Top && pbxP1.Left < pbxP2.Left + pbxP2.Width)
                    {
                        /*
                         * This IF statement checks that the player is within 7 pixels of the other player, if so it matches one of the criteria to stop falling.
                         */ 
                        if (pbxP1.Left + pbxP1.Width > pbxP2.Left && pbxP1.Top + pbxP1.Height - 2 > pbxP2.Top && pbxP1.Left < pbxP2.Left + pbxP2.Width)
                        {
                            /*
                             * If the criteria in this IF statement is met, the player's picture box has collided with the other picture box from the side as it has managed to
                             * reach below the set falling threshold of 7 pixels.
                             */ 
                            pbxP1.Top += 7; // Lowers the picture box by increasing the amount of pixels between the top of the picture box and the form.
                        }
                        else // If the picture box is within 7 pixels of the other player and is not 2 pixels into the top of the other player.
                        {
                            p1CurrentJump = playerAttributes[0, 3]; // Resets the amount of jumps left, determined by the respective player attribute.
                        }
                    
                    }
                    else
                    {
                        pbxP1.Top += 7; // Lowers the picture box by increasing the amount of pixels between the top of the picture box and the form.
                    }
                }
                else
                {
                    p1CurrentJump = playerAttributes[0, 3]; // Resets the amount of jumps left, determined by the respective player attribute.
                }
            }
            if (p1Left == true) // Checks if p1Left is true.
            {
                if (pbxP1.Left > 0) // Checks to make sure the picture box has not reached the left edge of the form.
                { 
                    pbxP1.Left -= 10; // Moves the picture box left by decreasing the amount of pixels between the left side of the picture box and the form.
                }
            }
            if (p1Right == true) // Checks if p1Right is true.
            {
                if (pbxP1.Left < 1269 - pbxP1.Width) // Checks to make sure the picture box has not reached the right edge of the form. 1269 is used instead of 1280 because several pixels are used for the forms border.
                {
                    pbxP1.Left += 10; // Moves the picture box right by increasing the amount of pixels between the left side of the picture box and the form.
                }
            }

            /*
             * The following code is the same as the code above except that it has been adjusted to use player two's picture box and attribute values instead.
             */
 
            if (p2Up == true) 
            {
                if (p2Force > 0)
                {
                    pbxP2.Top -= p2Force * 7;
                    p2Force--;
                }
                else
                {
                    p2Up = false;
                }
            }
            else
            {
                if (pbxP2.Top < 500)
                {
                    if (pbxP2.Left + pbxP2.Width > pbxP1.Left && pbxP2.Top + pbxP2.Height + 7 > pbxP1.Top && pbxP2.Left < pbxP1.Left + pbxP1.Width)
                    {
                        if (pbxP2.Left + pbxP2.Width > pbxP1.Left && pbxP2.Top + pbxP2.Height - 2 > pbxP1.Top && pbxP2.Left < pbxP1.Left + pbxP1.Width)
                        {
                            pbxP2.Top += 7;
                        }
                        else
                        {
                            p2CurrentJump = playerAttributes[1, 3];
                        }
                    }
                    else
                    {
                        pbxP2.Top += 7;
                    }
                }
                else
                {
                    p2CurrentJump = playerAttributes[1, 3];
                }
            }
            if (p2Left == true)
            {
                if (pbxP2.Left > 0)
                {
                    pbxP2.Left -= 10;
                }
            }
            if (p2Right == true)
            {
                if (pbxP2.Left < 1269 - pbxP2.Width)
                {
                    pbxP2.Left += 10;
                }
            }
        }
        #endregion
        #region Animate
        void Animate(int player, PictureBox pbxTarget)
        {
            /*
             * This method is responsible for animating the ghosts idle and parry animations.
             * The input integer player is a number which is used to represent which player's values are being used. 0 represents player one while 1 represents player two.
             * These numbers are used to represent the players as they are going to be used as indexes in arrays.
             * The input picture box's picture is the one that is going to be changed in this method.
             */ 

            pbxTarget.Image = animation[ghostState[player]][animationState[player]]; // Assigns the animation frame to the picture box.
            /* 
             * The integer representing the player is used as an index in ghostState to load the integer that is used as an index in the animation array. The
             * integer from ghostState is used to represent which set of animation frames is being used in the current animation.
             * A similar thing is done with the integer that is loaded from the animationState array. The integer that is loaded represents the current frame the animation
             * is up to.
             */

            if ((ghostState[player] == 0 || ghostState[player] == 1) && pbxTarget.Width != 114) // Checks if the ghostState value is neither 0 nor 1 and the width of the picture box is not 114
            {
                /*
                 * The ghostState integers 0 and 1 are the indexes for the ghost idle animation frames in the animation array.
                 * The picture box would be larger than 114 if the parry animation was recently used as the picture box is enlarged for that animation.
                 * If the ghostState value is 0 or 1, the ghost is idle and if the picture box is larger than 114 pixels it needs to be reset to 114.
                 */ 
                pbxTarget.Width = 114; // Sets the picture box's width to 114
            }

            if (animation[ghostState[player]].Length - 1 > animationState[player]) // Checks that the frame that the animation is currently on does not exceed the Length-1 of the array
            {
                // This check is necessary to ensure the index does not exceed the bounds of the array.
                animationState[player]++; // Increment the animationState value of the respective player.
            }
            else // If the index has exceeded the bounds of the array
            {
                animationState[player] = 0; // Resets the value (which is used as an index in the animation array) to 0 thereby resetting the animation frames to the first one.
            }
        }
        #endregion
        #region Key Up
        private void frmGame_KeyUp(object sender, KeyEventArgs e)
        {
            /*
             * This is an event handler for a key up event on the form. This is triggered when a key is released.
             */ 

            if (tmrIntro.Enabled == true) //Checks if the intro timer is active, if so, none of the key up events should be able to be triggered.
            {
                e.Handled = true; // Sets the Handled property of the KeyEventArgs to true, ensuring the key event does nothing.
            }
            else // If the timer is not enabled, the game has started, so key events are being handled.
            {
                switch (e.KeyCode) // Case statement which handleds the key event according to what key is released.
                {
                        /*
                         * The release of each key changes it's associated boolean to false.
                         */ 

                    case Keys.A:
                        {
                            p1Left = false;
                        }
                        break;
                    case Keys.D:
                        {
                            p1Right = false;
                        }
                        break;
                    case Keys.K:
                        {
                            p1Block = false;
                        }
                        break;
                    case Keys.Left:
                        {
                            p2Left = false;
                        }
                        break;
                    case Keys.Right:
                        {
                            p2Right = false;
                        }
                        break;
                    case Keys.V:
                        {
                            p2Block = false;
                        }
                        break;
                }
            }
        }
        #endregion
        #region Key Down
        private void frmGame_KeyDown(object sender, KeyEventArgs e)
        {
            /*
             * This is an event handler for a key down event on the form. This is triggered when a key is pushed down.
             */
            if (e.KeyCode == Keys.P)
            {
                MessageBox.Show(Convert.ToString(animationState[0]));
            }
            if (e.KeyCode == Keys.Escape)
            {
                finishClose = true;
                FormHolder.Menu.Show();
                this.Close();
            }
            if (tmrIntro.Enabled == true) //Checks if the intro timer is active, if so, none of the key down events should be able to be triggered.
            {
                e.Handled = true; // Sets the Handled property of the KeyEventArgs to true, ensuring the key event does nothing.
            }
            else // If the timer is not enabled, the game has started, so key events are being handled.
            {
                switch (e.KeyCode) // Case statement which handleds the key event according to what key is released.
                {
                    case Keys.W:
                        {
                            p1Up = true; // Sets p1Up to true.
                            if (p1CurrentJump > 0) // Checks if the player currently has any jumps left.
                            {
                                p1Force = 8; // Sets p1Force to 8, this is used in tmrMovement to move the picture box upwards.
                                p1CurrentJump--; // Decrements p1CurrentJump.
                            }
                        }
                        break;
                    case Keys.A:
                        {
                            p1Left = true; // Sets p1Left to true, this is used to move the picture box left.
                            p1LeftDirection = true; // Sets p1LeftDirection to true, this is used to determine which direction the animations face and the projectiles fire
                        }
                        break;
                    case Keys.D:
                        {
                            p1Right = true; // Sets p1Right to true, this is used to move the picture box right.
                            p1LeftDirection = false; // Sets p1LeftDirection to false, this is used to determine which direction the animations face and the projectiles fire
                        }
                        break;
                    case Keys.K:
                        {
                            if (tmrP1Parry.Enabled == false) // Checks to make sure the parry timer for this player is disabled, this is because the player should not be able to block while parrying.
                            {
                                p1Block = true; // Sets p1Block to true.
                            }
                        }
                        break;
                    case Keys.H:
                        {
                            if (tmrP1Parry.Enabled == false && p1Block == false) // Checks if parry timer is disabled and p1Block is false, this is so that fireballs can't be shot while parrying or blocking
                            {
                                if (pgbP1Stamina.Value >= 15) // Checks if there is adequate stamina to shoot the fireball.
                                {
                                    pgbP1Stamina.Value -= 15; // Decreases the progress bar's value by 15.
                                    Fireball(true, true, p1LeftDirection); // Calls the Fireball method passing through true, true and p1LeftDirection as the parameters. 
                                    /*
                                     * The first parameter represents the player firing the firball, true for player one and false for player two.
                                     * The second parameter represents whether the fireball is a light or heavy. True for light and false for heavy.
                                     * The third parameter is the bool p1LeftDirection, which determines which direction the fireball is being shot. True for left and false for right.
                                     */
                                }
                                else // If there isn't enough stamina.
                                {
                                    if (!tmrP1StaminaCooldown.Enabled)  // If the stamina cooldown timer is not enabled.
                                    {
                                        pgbP1Stamina.Value = 0; // Set stamina value to 0.
                                        tmrP1StaminaCooldown.Start(); // Start the tmrP1StaminaCooldown timer.
                                    }
                                }
                            }
                        }
                        break;
                    case Keys.J:
                        {
                            if (tmrP1Parry.Enabled == false && p1Block == false) // Checks if parry timer is disabled and p1Block is false, this is so that fireballs can't be shot while parrying or blocking
                            {
                                if (pgbP1Stamina.Value >= 25) // Checks if there is adequate stamina to shoot the fireball.
                                {
                                    pgbP1Stamina.Value -= 25; // Decreases the progress bar's value by 25.
                                    Fireball(true, false, p1LeftDirection); // Calls the Fireball method and passes through parameters
                                }
                                else // If there isn't enough stamina.
                                {
                                    if (!tmrP1StaminaCooldown.Enabled) // If the stamina cooldown timer is not enabled.
                                    {
                                        pgbP1Stamina.Value = 0; // Set stamina value to 0.
                                        tmrP1StaminaCooldown.Start(); // Start the tmrP1StaminaCooldown timer.
                                    }
                                }
                            }
                        }
                        break;
                    case Keys.L:
                        {
                            if (pgbP1Stamina.Value >= 40) // Checks if there is adequate stamina to parry.
                            {
                                pgbP1Stamina.Value -= 40; // Decreases the progress bar's value by 40.
                                p1Block = false; // Sets p1Block to false.
                                tmrP1Parry.Stop(); // Stops the parry timer in case it's already running.
                                tmrP1Parry.Start(); // Starts the parry timer.
                            }
                            else
                            {
                                if (!tmrP1StaminaCooldown.Enabled) // If the stamina cooldown timer is not enabled.
                                {
                                    pgbP1Stamina.Value = 0; // Set stamina value to 0.
                                    tmrP1StaminaCooldown.Start(); // Start the tmrP1StaminaCooldown timer.
                                }
                            }
                        }
                        break;
                    /*
                     * The following key events are the same actions, but for player two's keys and using their variables.
                     */

                    case Keys.Up:
                        {
                            p2Up = true;
                            if (p2CurrentJump > 0)
                            {
                                p2Force = 8;
                                p2CurrentJump--;
                            }
                        }
                        break;
                    case Keys.Left:
                        {
                            p2Left = true;
                            p2LeftDirection = true;
                        }
                        break;
                    case Keys.Right:
                        {
                            p2Right = true;
                            p2LeftDirection = false;
                        }
                        break;

                    case Keys.V:
                        {
                            if (tmrP2Parry.Enabled == false)
                            {
                                p2Block = true;
                            }
                        }
                        break;

                    case Keys.N:
                        {
                            if (tmrP2Parry.Enabled == false && p2Block == false)
                            {
                                if (pgbP2Stamina.Value >= 15)
                                {
                                    pgbP2Stamina.Value -= 15;
                                    Fireball(false, true, p2LeftDirection);
                                }
                                else
                                {
                                    if (!tmrP2StaminaCooldown.Enabled)
                                    {
                                        pgbP2Stamina.Value = 0;
                                        tmrP2StaminaCooldown.Start();
                                    }
                                }
                            }
                        }
                        break;
                    case Keys.B:
                        {
                            if (tmrP2Parry.Enabled == false && p2Block == false)
                            {
                                if (pgbP2Stamina.Value >= 25)
                                {
                                    pgbP2Stamina.Value -= 25;
                                    Fireball(false, false, p2LeftDirection);
                                }
                                else
                                {
                                    if (!tmrP2StaminaCooldown.Enabled)
                                    {
                                        pgbP2Stamina.Value = 0;
                                        tmrP2StaminaCooldown.Start();
                                    }
                                }
                            }
                        }
                        break;
                    case Keys.C:
                        {
                            if (pgbP2Stamina.Value >= 40)
                            {
                                pgbP2Stamina.Value -= 40;
                                p2Block = false;
                                tmrP2Parry.Stop();
                                tmrP2Parry.Start();
                            }
                            else
                            {
                                if (!tmrP2StaminaCooldown.Enabled)
                                {
                                    pgbP2Stamina.Value = 0;
                                    tmrP2StaminaCooldown.Start();
                                }
                            }
                        }
                        break;
                }
            }
        }
        #endregion
        #region Fireball
        private void Fireball(bool isP1, bool isLight, bool LeftDirection)
        {
            int fireballAnimationState = 0; // A local integer for determining the animation frame the fireball is currently on.
            bool endFireball = false; // Bool determining whether the fireball animation has ended.
            PictureBox pbxFireball = new PictureBox(); // Creates a new picture box.
            if (isLight == true) // Checks if the isLight input is true.
            {
                pbxFireball.Size = new Size(40, 40); // Sets the size of the picture box for a light fireball.
            }
            else
            {
                pbxFireball.Size = new Size(70, 70); // Sets the size of the picture box for a heavy fireball.
            }
            pbxFireball.BackColor = Color.Transparent; // Sets the back colour of the picture box to transparent.
            pbxFireball.SizeMode = PictureBoxSizeMode.Zoom; // Sets the size mode of the picture box to zoom, this will scale the picture according to the size of the picture box.
            if (LeftDirection == true) // Checks if LeftDirection is true.
            {
                pbxFireball.Image = animation[3][0]; // Sets the fireball image to one of the animation frames, this is done because there is a delay before the animation starts
                    // this will ensure the picture box isn't left without an image before the animation starts.

                if (isP1 == true) // Checks if isP1 is true, then sets the starting location of the fireball on the left side of the player firing it.
                {
                    pbxFireball.Location = new Point(pbxP1.Location.X - pbxFireball.Size.Width, pbxP1.Location.Y);
                }
                else
                {
                    pbxFireball.Location = new Point(pbxP2.Location.X - pbxFireball.Size.Width, pbxP2.Location.Y);
                }
            }
            else
            {
                pbxFireball.Image = animation[4][0];// Sets the fireball image to one of the animation frames, this is done because there is a delay before the animation starts
                // this will ensure the picture box isn't left without an image before the animation starts.

                if (isP1 == true) // Checks if isP1 is true, then sets the starting location of the fireball on the right side of the player firing it.
                {
                    pbxFireball.Location = new Point(pbxP1.Location.X + pbxFireball.Size.Width, pbxP1.Location.Y);
                }
                else
                {
                    pbxFireball.Location = new Point(pbxP2.Location.X + pbxFireball.Size.Width, pbxP2.Location.Y);
                }
            }
            Controls.Add(pbxFireball); // Adds the picture box to the form, making it visible.
            Timer tmrFireball = new Timer(); // Create a new timer to move the fireball.
            tmrFireball.Interval = 5; // Sets the interval for tmrFireball to 15 milliseconds.
            Timer tmrFireballAnimation = new Timer(); // Creates a timer for animating the fireball, this is created because it will run on a different interval than the movement timer.
            tmrFireballAnimation.Interval = 200; // Sets the interval for tmrFireballAnimation to 200 milliseconds.
            tmrFireball.Tick += (sender, eventArgs) => // A lambda expression is utilised to create an event handler for the timer tick event inside a method.
            {
                /*
                 * This event handler is responsible for moving the fireball in its set direction until there is a collsion with the opponent or it reaches the edge of the form. 
                 */
 
                if (endGame == false) //Checks if endGame is false. If it is, fireballs will not move to ensure there aren't anymore collisions which would result in multiple wins registering.
                {
                    if (pbxFireball.Left < 0 || pbxFireball.Left > 1280) // Checks to make sure the fireball has reached the edge of the form.
                    {
                        pbxFireball.Dispose(); //Removes the fireball from the form.
                        tmrFireball.Stop(); // Stops the fireball timer.
                        tmrFireballAnimation.Stop(); // Stops the firball animation timer.
                    }
                    else // If the fireball has not reached the edge of the form yet.
                    {
                        if (LeftDirection == true) // If LeftDirection is true.
                        {
                            pbxFireball.Left -= 10; // Moves the fireball left.
                        }
                        else
                        {
                            pbxFireball.Left += 10; // Moves the fireball right.
                        }
                    }
                    if (isP1 == true) // Checks if isP1 is true
                    {
                        if (CollisionCheck(pbxP2, pbxFireball) == true) // Passes pbxP2 and pbxFireball into the Collision Check method which detects whether or not there is a collision, and returns the appropriate bool value.
                        {
                            tmrFireball.Stop(); // Stops the fireball timer.
                            fireballAnimationState = 3; // Sets fireballAnimationState to 3, which moves it out of the flying animation cycle and allows the impact animation to start.
                            endFireball = true; // Sets endFireball to true.

                            if (tmrP2Parry.Enabled == true) // Checks if the other player's parry timer is active.
                            {
                                Parry(false, true, isLight); // Passes through parameters into the Parry method.
                                /*
                                 * The first parameter is boolean which represents which player is parrying. True for player one and false for player two.
                                 * The second parameter is a bool which represents whether the projectile being parried is a fireball or not. True for fireball and false for ice shard.
                                 * The third parameter is an optional parementer which represent whether the fireball is light or not. True for light, false for heavy. The variable is used in
                                 * this method so the variable is passed through.
                                 */ 
                            }
                            else // If the player is not parrying.
                            {
                                if (p2Block == true) // Check if the player is blocking.
                                {
                                    FireballBlock(false, isLight);
                                }
                                else // If the player is not parrying or blocking.
                                {
                                    Collision(false, true, isLight);
                                }
                            }

                        }
                    }
                    else
                    {
                        /*
                         * The following code is the same as the IF statement above except the variables have been changed for the other player.
                         */ 
                        if (CollisionCheck(pbxP1, pbxFireball) == true)
                        {
                            tmrFireball.Stop();
                            fireballAnimationState = 3;
                            endFireball = true;
                            if (tmrP1Parry.Enabled == true)
                            {
                                Parry(true, true, isLight);
                            }
                            else
                            {
                                if (p1Block == true)
                                {
                                    FireballBlock(true, isLight);
                                }
                                else
                                {
                                    Collision(true, true, isLight);
                                }
                            }

                        }
                    }
                }
            };
            tmrFireballAnimation.Tick += (sender, eventArgs) => // A lambda expression is utilised to create an event handler for the timer tick event inside a method.
                {
                    /*
                     * This event handler is responsible for animating the fireball while it is flying and then once there is an impact with a player, it should
                     * animate the impact animation.
                     */ 

                    if (LeftDirection == true) // Checks the bool LeftDirection and uses the image array for the corresponding direction.
                    {
                        pbxFireball.Image = animation[3][fireballAnimationState]; // Sets the image of the fireball picture box.
                    }
                    else
                    {
                        pbxFireball.Image = animation[4][fireballAnimationState]; // Sets the image of the fireball picture box.
                    }

                    if (fireballAnimationState < 2) // Checks if fireballAnimationState is less than 2.
                    {
                        fireballAnimationState++; // Increment fireballAnimationState.
                    }
                    else // If fireballAnimationState is 3, then the impact animation will start.
                    {
                        if (fireballAnimationState > 2 && fireballAnimationState < 7) // If fireballAnimationState is larger than 2 and less than 7.
                        {
                            fireballAnimationState++; // Increment fireballAnimationState.
                            tmrFireballAnimation.Interval = 50; // Set the animation interval to 50, this is to speed up the animation for the impact.
                        }
                        else // Reset Animation, this is reached when an animation has reached the end of a cycle.
                        {
                            if (endFireball == true) // If the impact animation has finished.
                            {
                                tmrFireballAnimation.Stop(); // Stops the animation timer.
                                pbxFireball.Dispose(); // Removes the fireball picture box from the form.
                            }
                            else // If endFireball is not true, then the flying animation is still playing.
                            {
                                fireballAnimationState = 0; // Sets fireballAnimationState to 0, thereby restarting the animation cycle.
                            }
                        }
                    }
                };
                tmrFireballAnimation.Start(); // Starts the fireball animation timer.
                tmrFireball.Start(); // Starts the fireball timer.
        }
        #endregion
        #region Timers: Parry, Stamina, Stamina Cooldown, Game time, Intro
        private void tmrP1Parry_Tick(object sender, EventArgs e)
        {
            /*
             * Parry timer, when this timer is active, projectiles that collide with the corresponding player's picture box will be parried.
             * Once the parry timer has ticked once, it will deactivate. The timer also resets the animationState of the player to 0 to ensure it is within the bounds of array which holds the idle frames.
             */
            tmrP1Parry.Stop(); // Stops this timer.
            ResetIdle(true); // Calls ResetIdle method to reset the ghost animation from parry to idle. The integet that is passed as a parameter is representative of the player.
            animationState[0] = 0; // Reset animationState value for player one to 0.
        }

        private void tmrP2Parry_Tick(object sender, EventArgs e)
        {
            /*
             * Parry timer, when this timer is active, projectiles that collide with the corresponding player's picture box will be parried.
             * Once the parry timer has ticked once, it will deactivate. The timer also resets the animationState of the player to 0 to ensure it is within the bounds of array which holds the idle frames.
             */
            tmrP2Parry.Stop(); // Stops this timer.
            ResetIdle(false); // Calls ResetIdle method to reset the ghost animation from parry to idle. The integet that is passed as a parameter is representative of the player.
            animationState[1] = 0; // Reset animationState value for player one to 0.
        }

        private void tmrP1StaminaCooldown_Tick(object sender, EventArgs e)
        {
            /*
             * When this timer is active, it will be detected and stamina will no regenerate until is is disabled.
             * This timer will tick once before it is disabled.
             */
            tmrP1StaminaCooldown.Stop(); // Stops this timer.
        }
        private void tmrP2StaminaCooldown_Tick(object sender, EventArgs e)
        {
            /*
             * When this timer is active, it will be detected and stamina will no regenerate until is is disabled.
             * This timer will tick once before it is disabled.
             */
            tmrP2StaminaCooldown.Stop(); // Stops this timer.
        }

        private void tmrStamina_Tick(object sender, EventArgs e)
        {
            /*
             * This timer is responsible for regenerating the stamina of both players.
             */ 
            if (!tmrP1StaminaCooldown.Enabled) // If tmrP1StaminaCooldown is not enabled.
            {
                if (p1Block == false) // If player is not blocking, as players will no regenerate stamina when blocking.
                {
                    if (pgbP1Stamina.Value + (playerAttributes[0, 1] +1) <= 100) // If the amount to be regenerated is less than or equal to 100.
                    {
                        pgbP1Stamina.Value += playerAttributes[0, 1] +1; // Increase the progress bar value by the respective attribute plus 1.
                    }
                    else // If there is more stamina than what is going to be regenerated this tick.
                    {
                        pgbP1Stamina.Value = 100; // Set the progress bar value to 100.
                    }
                }
            }
            if (!tmrP2StaminaCooldown.Enabled) // If tmrP2StaminaCooldown is not enabled.
            {
                if (p2Block == false) // If player is not blocking, as players will no regenerate stamina when blocking.
                {
                    if (pgbP2Stamina.Value + (playerAttributes[1, 1] + 1) <= 100) // If the amount to be regenerated is less than or equal to 100.
                    {
                        pgbP2Stamina.Value += playerAttributes[1, 1] +1; // Increase the progress bar value by the respective attribute plus 1.
                    }
                    else // If there is more stamina than what is going to be regenerated this tick.
                    {
                        pgbP2Stamina.Value = 100; // Set the progress bar value to 100.
                    }
                }
            }
        }
        private void tmrGameTime_Tick(object sender, EventArgs e)
        {
            /*
             * Time tick event handler for tmrGameTime. This timer counts down from 99, once it hits 0 the game will end and the player with more health wins. 
             */ 

            lblGameTime.Text = Convert.ToString(gameTime); // Displays the game time by converting it to a string and displaying it in a label.
            if (gameTime == 0) // If gameTime is 0.
            {
                if (pgbP1Health.Value == pgbP2Health.Value) // If the value of both progress bars are the same
                {
                    Win(0); // Calls Win method and passes in an integer representing who won. 0 is a draw, 1 is player one's win, and 2 is player two's win.
                }
                else
                {
                    if (pgbP1Health.Value > pgbP2Health.Value) // If the value for pgbP1Health is larger than pgbP2Health.
                    {
                        Win(1); // Player one wins.
                    }
                    else // If player two has more health.
                    {
                        Win(2); // Player two wins.
                    }
                }
            }
            gameTime--; // Decrement gameTime.
        }

        private void tmrIntro_Tick(object sender, EventArgs e)
        {
            /*
             * Time tick event handler for tmrIntro. This timer counts down from 3 seconds before the game starts.
             */ 

            lblCountdown.Text = Convert.ToString(introCountdown); // Displays introCountdown by converting it to string and displaying in a label.
            if (introCountdown == 0) // If introCountdown is 0.
            {
                tmrIntro.Stop(); // Stop tmrIntro.
                lblCountdown.Dispose(); // Removes Countdown label from form.
                tmrGameTime.Start(); // Starts the timer for game time.
            }
            else
            {
                introCountdown--; // Decrement introCountdown.
            }
        }
        #endregion
        #region Collision Check + Collision
        bool CollisionCheck(PictureBox pbxPlayer, PictureBox pbxProjectile)
        {
            /*
             * This method receieves two picture boxes as parameters and checks if they have overlapped, if so it'll return a true value. If not, it'll return false.
             */ 
                   //right collision                                                   bottom collision                                            left collision                                          top collision
            if (pbxPlayer.Left + pbxPlayer.Width > pbxProjectile.Left && pbxPlayer.Top + pbxPlayer.Height > pbxProjectile.Top && pbxPlayer.Left < pbxProjectile.Left + pbxProjectile.Width && pbxPlayer.Top < pbxProjectile.Top + pbxProjectile.Height)
            {
                /*
                 * Checks if the picture box has overlapped with the other by comparing the top and bottom values with each other and adding or subtracting the width or height 
                 * as needed to.
                 */ 
                return true; // Returns true if there is a collision.
            }
            else
            {
                return false; // Returns false if there is no collision.
            }
        }
        void Collision(bool isP1, bool isFireball, bool isLight = false)
        {
            /*
             * This method is responsible for taking inputs on what player is getting hit and what projectile they are being hit by. This is used to called
             * the Damage method while passing through respective parameters.
             */

            if (isP1 == true) // If 
            {
                if (isFireball == true)
                {
                    if (isLight == true)
                    {
                        Damage(pgbP1Health, 5, playerAttributes[1, 0], 2); // Calls the Damage method while passing through parameters defined by the IF statements.
                        /*
                         * The Damage method takes in 4 arguments.
                         * The first one is the progress bar that is going to have it's value decreased in the case that the player is not blocking or parrying.
                         * The second one is the base damage, this is going to be multiplied by the multiplier which is the damage attribute.
                         * The third one is the player attribute value which acts as the multiplier for the damage given.
                         * The fourth parameter is an integer which represents the player and will be passed onto the Win method if the progress bar reaches a value of 0.
                         */ 
                    }
                    else // If isLight is false.
                    {
                        Damage(pgbP1Health, 10, playerAttributes[1, 0], 2); // Calls the Damage method while passing through parameters defined by the IF statements.
                    }

                }
                else // If isFireball is false.
                {
                    Damage(pgbP1Health, 15, playerAttributes[0, 0], 2); // Calls the Damage method while passing through parameters defined by the IF statements.
                }
            }
            else // If isP1 is false.
            {
                if (isFireball == true)
                {
                    if (isLight == true)
                    {
                        Damage(pgbP2Health, 5, playerAttributes[0, 0], 1); // Calls the Damage method while passing through parameters defined by the IF statements.
                    }
                    else // If isLight is false.
                    {
                        Damage(pgbP2Health, 10, playerAttributes[0, 0], 1); // Calls the Damage method while passing through parameters defined by the IF statements.
                    }
                }
                else // If isFireball is false.
                {
                    Damage(pgbP2Health, 15, playerAttributes[1, 0], 1); // Calls the Damage method while passing through parameters defined by the IF statements.
                }
            }
        }
        #endregion
        #region Overlap Checker
        void OverlapChecker()
        {
            if (CollisionCheck(pbxP1, pbxP2) == true || CollisionCheck(pbxP2, pbxP1) == true) // Passes both picture boxes through the Collision Check method, both criteria are not necessary, just a precaution.
            {
                if (pbxP1.Left > pbxP2.Left) // If pbxP1.Left is bigger that pbxP2.Left than pbxP1 is colliding into pbxP2's right side. Movement is disabled accordingly.
                {
                    p1Left = false; // Disables p1Left to stop pbxP1 from moving left any further.
                    p2Right = false; // Disables p2Right to stop pbxP2 from moving right any further.

                }
                else
                {
                    if (pbxP1.Left < pbxP2.Left) // If pbxP2.Left is bigger that pbxP1.Left than pbxP1 is colliding into pbxP2's left side. Movement is disabled accordingly.
                    {
                        p1Right = false; // Disables p1Right to stop pbxP1 from moving right any further.
                        p2Left = false; // Disables p2Left to stop pbxP2 from moving left any further.
                    }
                }
            }
        }
        #endregion
        #region Damage
        void Damage(ProgressBar pgbHealth, int baseDamage, int multiplier, int win)
        {
            /*
             * This methods processes the damage and reduces the input progress bar. If it has reached 0, will call the win method.
             */ 
            if (pgbHealth.Value > baseDamage * multiplier) // If the value of the damage is smaller than the progress bar value, this is to make sure the value doesn't drop below 0.
            {
                pgbHealth.Value -= baseDamage * multiplier; // Decreases the progress bar value by the damage.
            }
            else // If the damage exceeds the progress bar value. This is necessary because if the progress bar value drops below 0, there will be an error.
            {
                pgbHealth.Value = 0; // Sets the progress bar value to 0, this is only so it shows the health bar entirely depleted when the message box about the player winning appears.
                Win(win); // Calls the win method and passes in the integer representing the player.
            }
        }
        #endregion
        #region Stamina Cost
        void StaminaCost(ProgressBar pgbHealth, ProgressBar pgbStamina, int baseValue, int multiplier, Timer tmrStaminaCooldown)
        {
            /*
             * This method depletes stamina based on a base value times a multiplier. This is used for blocking as the excess decreases health.
             * The inputs are progress bar of the health of the player blocking, progress bar of the stamina, integer of the base damage, integer of the 
             * damage multiplier, and the stamina cooldown timer of the player.
             */ 
            if (pgbStamina.Value >= baseValue * multiplier) // Checks to see if there is adequate stamina based on the baseValue times mulitplier.
            {
                pgbStamina.Value -= baseValue * multiplier; // If there is enough stamina, deduct the value it from the progress bar value.
            }
            else // If there is not enough stamina.
            {
                if (pgbHealth.Value > (baseValue * multiplier) - pgbStamina.Value) // Checks if there is enough health for the excess damage.
                {
                    pgbHealth.Value -= (baseValue * multiplier) - pgbStamina.Value; // Decreases the health of the player by the amount of the excess damage.
                    pgbStamina.Value = 0; // Sets the stamina value to 0.
                    tmrStaminaCooldown.Start(); // Starts the stamina cooldown timer.
                }
                else // If there is not enough health and stamina. 
                {
                    pgbHealth.Value = 0; // Sets the player's health to 0.
                    if(pgbP1Health.Value > pgbP2Health.Value) // Checks which player has more health and passes the winner's corresponding number through to the win method.
                    {
                        Win(1);
                    }
                    else
                    {
                        Win(2);
                    }
                }
            }
        }
        #endregion
        #region Win
        void Win(int winner)
        {
            /*
             * This method takes an integer as an input. This integer represents the player which has won. 0 for a draw, 1 for player 1, 2 for player 2.
             * This displays the winner then closes the form and re-opens the main menu.
             */ 
            endGame = true; // Sets endGame to true. If endGame is true, fireballs will cease movement, this means more collisions will be avoided, thereby avoiding this method being called multiple times.
            tmrGameTime.Stop(); // Stops the game timer.
            switch (winner) // Tests the value of winner.
            {
                case 0:
                    {
                        MessageBox.Show("Draw! \nPress Ok to return to menu"); // Displays the appropriate message in a message box.
                    }
                    break;
                case 1:
                    {
                        MessageBox.Show("Player 1 Wins! \nPress Ok to return to menu"); // Displays the appropriate message in a message box.
                    }
                    break;
                case 2:
                    {
                        MessageBox.Show("Player 2 Wins! \nPress Ok to return to menu"); // Displays the appropriate message in a message box.
                    }
                    break;
            }
            // The code below won't run until the message box is closed.
            FormHolder.Menu.Show(); // Shows the main menu form.
            finishClose = true; // Sets finishClose to true. If this is true and the form is closed, the applpication won't exit.
            this.Close(); // Closes the form.
        }
        #endregion
        #region FireballBlock
        void FireballBlock(bool isP1, bool isLight)
        {
            /*
             * This method takes two bools as inputs. The first bool represents the player who is blocking, the second bool represents
             * whether the fireball is light. This method checks whether isP1 and isLight is true, depending on what value each bool is, different parameters will be passed
             * through the StaminaCost method.
             */ 
            if (isP1 == true)
            {
                if (isLight == true)
                {
                    StaminaCost(pgbP1Health, pgbP1Stamina, 5, playerAttributes[1, 0], tmrP1StaminaCooldown);
                    /*
                     * Passes the respective progress bars, timer and values into the stamina cost method.
                     */ 
                }
                else
                {
                    StaminaCost(pgbP1Health, pgbP1Stamina, 10, playerAttributes[1, 0], tmrP1StaminaCooldown);
                }
            }
            else
            {
                if (isLight == true)
                {
                    StaminaCost(pgbP2Health, pgbP2Stamina, 5, playerAttributes[0, 0], tmrP2StaminaCooldown);
                }
                else
                {
                    StaminaCost(pgbP2Health, pgbP2Stamina, 10, playerAttributes[0, 0], tmrP2StaminaCooldown);
                }
            }
        }
        #endregion
        #region Parry
        void Parry(bool isP1, bool isFireball, bool isLight = false)
        {
            /*
             * This method takes 3 bools as inputs. The first one represents the player that has parried, the second represents whether the 
             * projectile that was parried is a fireball or not. The optional bool represents whether the fireball was light or not.
             */
 
            if (isP1 == true) // If player one was the one that parried.
            {
                if (isFireball == true) // If isFireball is true.
                {
                    if (isLight == true) // If isLight is true.
                    {
                        Ice(true, 0);
                        Ice(true, -5);
                        Ice(true, 5);
                        /*
                         * Calls Ice method, passing in a boolean that represents the player firing the ice shards, and the integer value
                         * used as the rise value. This value determines how high the ice shard moves every tick.
                         */ 
                    }
                    else // If isLight is false.
                    {
                        if (pgbP1Health.Value <= 85) // If the player's health is less than or equal to 85.
                        {
                            pgbP1Health.Value += 15; // Add 15 to the health value.
                        }
                        else
                        {
                            pgbP1Health.Value = 100; // Set the player's health value to 100.
                        }
                        if (pgbP1Stamina.Value <= 80) // If the player's stamina is less than or equal to 80.
                        {
                            pgbP1Stamina.Value += 20; // Add 20 to the player's stamina value.
                        }
                        else
                        {
                            pgbP1Stamina.Value = 100; // Set the stamina value to 100.
                        }
                        if (playerAttributes[0,2] == 4) // If the player's parry attribute is maxed, shoot ice projectiles.
                        {
                            Ice(true, 0);
                            Ice(true, -5);
                            Ice(true, 5);
                        }
                    }

                }
                else // If the projectile that was parried was an ice shard.
                {
                    Ice(true, 0);
                    Ice(true, -5);
                    Ice(true, 5);
                }
            }
            else // The else is for player two and the code is the same as player one but with player two's values.
            {
                if (isFireball == true)
                {
                    if (isLight == true)
                    {
                        Ice(false, 0);
                        Ice(false, -5);
                        Ice(false, 5);
                    }
                    else
                    {
                        if (pgbP2Health.Value <= 85)
                        {
                            pgbP2Health.Value += 15;
                        }
                        else
                        {
                            pgbP2Health.Value = 100;
                        }
                        if (pgbP2Stamina.Value <= 80)
                        {
                            pgbP2Stamina.Value += 20;
                        }
                        else
                        {
                            pgbP2Stamina.Value = 100;
                        }
                        if (playerAttributes[1, 2] == 4)
                        {
                            Ice(false, 0);
                            Ice(false, -5);
                            Ice(false, 5);
                        }
                    }

                }
                else
                {
                    Ice(false, 0);
                    Ice(false, -5);
                    Ice(false, 5);
                }
            }


        }
        #endregion
        #region Ice
        void Ice(bool isP1, int rise)
        {
            /*
             * This method creates and moves the ice shard. The firs boolean input determine the player which is shooting the ice shard. The 
             * second input is used to determine how high the ice shard rises each tick on the ice shard's timer.
             */ 
            PictureBox pbxIce = new PictureBox(); // Creates a new picture box for the ice shard.
            pbxIce.Size = new Size(30, 30); // Sets the size of the picture box.
            pbxIce.BackColor = Color.Transparent; // Sets the back colour of the picture box to transparent.
            pbxIce.SizeMode = PictureBoxSizeMode.Zoom; // Sets the size mode of the picture box to zoom, this will rescale the ice shard as needed.
            /*
             * These IF statements determine what LeftDirection's value is as well as set the starting location of the ice picture box.
             */
            bool LeftDirection;
            if (isP1 == true)
            {
                if (p1LeftDirection == true)
                { 
                    LeftDirection = true;
                    pbxIce.Location = new System.Drawing.Point(pbxP1.Location.X - 30, pbxP1.Location.Y);
                }
                else
                {
                    pbxIce.Location = new System.Drawing.Point(pbxP1.Location.X + 30, pbxP1.Location.Y);
                    LeftDirection = false;
                }
            }
            else
            {
                if (p2LeftDirection == true)
                {
                    pbxIce.Location = new System.Drawing.Point(pbxP2.Location.X - 30, pbxP2.Location.Y);
                    LeftDirection = true; 
                }
                else
                {
                    pbxIce.Location = new System.Drawing.Point(pbxP1.Location.X + 30, pbxP1.Location.Y);
                    LeftDirection = false;
                }
            }
            /*
             * This IF statement assigns the correct image to the picture box.
             */ 
            if (LeftDirection == true)
            {
                switch (rise)
                {
                    case -5:
                        {
                            pbxIce.Image = animation[5][0];
                        }
                        break;
                    case 0:
                        {
                            pbxIce.Image = animation[5][1];
                        }
                        break;
                    case 5:
                        {
                            pbxIce.Image = animation[5][2];
                        }
                        break;
                }
                
            }
            else
            {
                switch (rise)
                {
                    case -5:
                        {
                            pbxIce.Image = animation[6][2];
                        }
                        break;
                    case 0:
                        {
                            pbxIce.Image = animation[6][1];
                        }
                        break;
                    case 5:
                        {
                            pbxIce.Image = animation[6][0];
                        }
                        break;
                }
                

            }
            Controls.Add(pbxIce); // Adds the picture box to the form.
            Timer tmrIce = new Timer(); // Creates a new timer to move the picture box.
            tmrIce.Interval = 5; // Sets the interval of the timer.
            tmrIce.Tick += (sender, eventArgs) => // Lambda expression to create an event handler for the timer inside a method.
                {
                    if (pbxIce.Left > 1200 || pbxIce.Left < 0 || pbxIce.Top < 0 || pbxIce.Top > 800) // If the picture box has reached the edge of the form.
                    {
                        pbxIce.Dispose(); // Remove the picture box from the form.
                        tmrIce.Stop(); // Stops the timer.
                    }
                    else // If the picture box has not reached the edge of the form.
                    {
                        if (LeftDirection == true) // Checks if LeftDirection is true.
                        {
                            /*
                             * Moves the picture box
                             */
                            pbxIce.Left -= 10;
                            pbxIce.Top += rise;
                        }
                        else
                        {
                            pbxIce.Left += 10;
                            pbxIce.Top -= rise;
                        }
                    }

                    
                    if (isP1 == true)
                    {
                        if (CollisionCheck(pbxP2, pbxIce) == true) // Passes the opponent's picture box and the ice picture box through the CollisionCheck method.
                        {
                            pbxIce.Dispose(); // Removes the picture box from the form.
                            tmrIce.Stop(); // Stops the timer. 
                            if (tmrP2Parry.Enabled == true) // If the opponent is parrying.
                            {
                                Parry(false, false); // Passes the bools representing the player parrying and the if the projectile is a fireball into the parry method.
                            }
                            else
                            {
                                Collision(false, false); // Passes the bools representing the player being hit and the if the projectile is a fireball into the collision method.
                            }
                        }
                    }
                    else // The code in the else is similar to the code in the IF statement.
                    {
                        if (CollisionCheck(pbxP1, pbxIce) == true)
                        {
                            pbxIce.Dispose();
                            tmrIce.Stop();
                            if (tmrP1Parry.Enabled == true)
                            {
                                Parry(true, false);
                            }
                            else
                            {
                                Collision(true, false);
                            }
                        }
                    }
                };
            tmrIce.Start(); // Starts the ice timer.
        }
        #endregion
        #region Animation Timer
        private void tmrAnimation_Tick(object sender, EventArgs e)
        {
            /*
             * This timer event handler is responsible for animating the ghosts.
             */ 
            if (p1Block == true) // Checks if p1Block is true.
            {
                ghostState[0]= 2; // Changes the ghostState of player one to 2, which will be used as the index for the animation array.
                if (p1LeftDirection == true) // Checks if p1LeftDirection is true.
                {
                    animationState[0] = 0; // Changes the animation state for player one to 0. This determines the direction the ghost blocking frame is facing.
                }
                else
                {
                    animationState[0] = 1; // Changes the animation state for player one to 0.
                }
                pbxP1.Image = animation[ghostState[0]][animationState[0]]; // Assigns the appropriate picture to the picture box.
            }
            else // If the player is not blocking.
            {
                if (tmrP1Parry.Enabled == true) // If the player is parrying.
                {
                    animationState[0] = 0; // Resets the animation state to 0. This determines which frame the animation is on. 
                    if (pbxP1.Width != 151) // If the width of the picture box is not 151, which is the enlarged version for the parry.
                    {
                        pbxP1.Width = 151; // Sets the picture box width to 151.
                    }
                    if (p1LeftDirection == true) // Checks if left direction is true.
                    {
                        ghostState[0] = 7; // Sets the ghostState to 7, this is used in the animation to determine the set of frames to be used.
                    }
                    else
                    {
                        ghostState[0] = 8; // Sets the ghostState of player 1 to 8.
                    }
                }
                else
                {
                    ResetIdle(true); // Calls the reset idle method and passes through an integer representing the player. 0 for player 1 and 1 for player 2.
                }
                Animate(0, pbxP1); // Calls the animate method, which is responsible for animating the idle animation. Passes through an bool representing the player and the player's picture box.
            }

            /*
             * The code below is similar to the code above but for player two so player two's variables are used.
             */ 
            if (p2Block == true)
            {
                ghostState[1] = 2;
                if (p2LeftDirection == true)
                {
                    animationState[1] = 0;
                }
                else
                {
                    animationState[1] = 1;
                }
                pbxP2.Image = animation[ghostState[1]][animationState[1]];
            }
            else
            {
                if (tmrP2Parry.Enabled == true)
                {
                    if (pbxP2.Width != 151)
                    {
                        pbxP2.Width = 151;
                    }
                    if (p2LeftDirection == true)
                    {
                        ghostState[1] = 7;
                    }
                    else
                    {
                        ghostState[1] = 8;
                    }
                }
                else
                {
                    ResetIdle(false);
                }
                Animate(1, pbxP2);
            }
        }
        #endregion
        #region Reset Idle
        void ResetIdle(bool isP1)
        {
            /*
             * This method is responsible for changing the ghostState back the idle.
             */ 
            if (isP1 == true) // Checks if isP1 is true.
            {
                if (p1LeftDirection == true) // Checks if p1LeftDirecton is true.
                {
                    ghostState[0] = 0; // Sets ghostState to 0
                }
                else
                {
                    ghostState[0] = 1; // Sets ghostState to 1
                }
            }
            else
            {
                if (p2LeftDirection == true) // Checks if p2LeftDirecton is true.
                {
                    ghostState[1] = 0; // Sets ghostState to 0
                }
                else
                {
                    ghostState[1] = 1; // Sets ghostState to 1
                }
            }
        }
        #endregion
        #region Form Closing
        private void frmGame_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (finishClose == false) // Checks if finishClose is false. finishClose is true when the form is being closed and to return to the main menu.
            {
                Application.Exit(); // Closes the application.
            }
        }
        #endregion
    }
}
